<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Model;

use Dividebuy\Common\Utility\StoreConfigHelper;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class DividebuyConfigProvider.
 */
class DividebuyConfigProvider implements ConfigProviderInterface
{
  protected StoreConfigHelper $_retailerConfigHelper;

  public function __construct(StoreConfigHelper $retailerConfigHelper)
  {
    $this->_retailerConfigHelper = $retailerConfigHelper;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig()
  {
    return [
        'payment' => [
            'dividebuy' => [
                'imageSrc' => $this->getImageSrc(),
            ],
        ],
    ];
  }

  /**
   * Used to get checkout banner URL.
   *
   * @return string
   *
   * @throws NoSuchEntityException
   */
  public function getImageSrc()
  {
    return $this->_retailerConfigHelper->getCheckoutBannerUrl();
  }
}
