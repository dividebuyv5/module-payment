<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Model\Carrier;

use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Utility\SessionHelper;
use Magento\Checkout\Model\Session;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\Method;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Rate\ResultFactory;

class Dividebuyshipping extends AbstractCarrier implements CarrierInterface
{
  /**
   * @var string
   */
  protected $_code = DivideBuy::SHIPPING_CODE;

  protected ResultFactory $rateResultFactory;

  protected MethodFactory $rateMethodFactory;

  protected Session $checkoutSession;

  public function __construct(
      SessionHelper $sessionHelper,
      ErrorFactory $rateErrorFactory,
      ResultFactory $rateResultFactory,
      MethodFactory $rateMethodFactory,
      array $data = []
  ) {
    $this->rateResultFactory = $rateResultFactory;
    $this->rateMethodFactory = $rateMethodFactory;
    $this->checkoutSession = $sessionHelper->getCheckoutSession();
    $configHelper = $sessionHelper->getStoreConfigHelper();

    parent::__construct(
        $configHelper->getScopeConfig(),
        $rateErrorFactory,
        $configHelper->getLogger(),
        $data
    );
  }

  /**
   * @return array
   */
  public function getAllowedMethods()
  {
    return [DivideBuy::SHIPPING_CODE => $this->getConfigData('name')];
  }

  /**
   * @param  RateRequest  $request
   *
   * @return bool|Result
   */
  public function collectRates(RateRequest $request)
  {
    if (!$this->getActiveFlag()) {
      // This shipping method is disabled in the configuration
      return false;
    }

    return $this->collectRatesWhenActive();
  }

  /**
   * @return bool|Result
   */
  private function collectRatesWhenActive()
  {
    /** @var Method $method */
    $result = $this->rateResultFactory->create();

    $method = $this->rateMethodFactory->create();

    $method->setCarrier(DivideBuy::SHIPPING_CODE);
    // Get the title from the configuration, as defined in system.xml
    $method->setCarrierTitle($this->getConfigData('title'));

    $method->setMethod(DivideBuy::SHIPPING_CODE);
    // Get the title from the configuration, as defined in system.xml
    $method->setMethodTitle($this->getConfigData('name'));

    // Get the price from the configuration, as defined in system.xml
    $amount = $this->getConfigData('price');

    $method->setPrice($amount);
    $method->setCost($amount);

    $divideBuyPosQuoteId = $this->checkoutSession->getDividebuyCustomQuoteId();

    if ($divideBuyPosQuoteId) {
      $result->append($method);
    }

    return $result;
  }
}
