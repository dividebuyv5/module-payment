<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Model;

use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Utility\CartHelper;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Dividebuy\Payment\Block\Form\Dbpayment as DbPaymentBlock;
use Magento\Framework\Api\AttributeValueFactory;
use Magento\Framework\Api\ExtensionAttributesFactory;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\DataObject;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\UrlInterface;
use Magento\Payment\Block\Info\Instructions;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Payment\Model\Method\Logger;
use Magento\Quote\Api\Data\CartInterface;

/**
 * Cash on delivery payment method model.
 */
class Dbpayment extends AbstractMethod
{
  /**
   * Payment method code.
   *
   * @var string
   */
  protected $_code = DivideBuy::DIVIDEBUY_PAYMENT_CODE;

  /**
   * Cash On Delivery payment block paths.
   *
   * @var string
   */
  protected $_formBlockType = DbPaymentBlock::class;

  /**
   * Info instructions block path.
   *
   * @var string
   */
  protected $_infoBlockType = Instructions::class;

  /**
   * Availability option.
   *
   * @var bool
   */
  protected $_isOffline = true;

  private StoreConfigHelper $configHelper;

  private CartHelper $cartHelper;

  public function __construct(
      StoreConfigHelper $configHelper,
      CartHelper $cartHelper,
      Context $context,
      Registry $registry,
      ExtensionAttributesFactory $extFactory,
      AttributeValueFactory $attribFactory,
      Data $paymentData,
      Logger $logger
  ) {
    parent::__construct(
        $context,
        $registry,
        $extFactory,
        $attribFactory,
        $paymentData,
        $configHelper->getScopeConfig(),
        $logger
    );

    $this->configHelper = $configHelper;
    $this->cartHelper = $cartHelper;
  }

  public function getOrderPlaceRedirectUrl()
  {
    $objectManager = ObjectManager::getInstance();
    $urlBuilder = $objectManager->get(UrlInterface::class);

    return $urlBuilder->getUrl('dividebuy/payment/redirect');
  }

  /**
   * Get instructions text from config.
   *
   * @return string
   */
  public function getInstructions()
  {
    return trim($this->getConfigData('instructions'));
  }

  /**
   * Check whether payment method can be used.
   *
   * @param  CartInterface|null  $quote
   *
   * @return bool
   */
  public function isAvailable(?CartInterface $quote = null)
  {
    // Checking if quote is null.
    if (empty($quote)) {
      return false;
    }

    $isIpAllowed = $this->configHelper->isIpAllowed($quote->getStoreId());
    $extensionStatus = $this->configHelper->getExtensionStatus($quote->getStoreId());
    $divideBuyItems = $this->cartHelper->getItemArray($quote->getId());

    if (!$this->isActive($quote ? $quote->getStoreId() : null) ||
      !$isIpAllowed || !$extensionStatus
      || empty($divideBuyItems['dividebuy'])) {
      return false;
    }

    // Appending DivideBuy payment method to checkout.
    $checkResult = new DataObject();
    $checkResult->setData('is_available', true);

    // for future use in observers
    $this->_eventManager->dispatch(
        'payment_method_is_active',
        [
            'result' => $checkResult,
            'method_instance' => $this,
            'quote' => $quote,
        ]
    );

    return $checkResult->getData('is_available');
  }
}
