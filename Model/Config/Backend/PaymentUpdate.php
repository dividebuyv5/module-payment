<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Model\Config\Backend;

use Dividebuy\Common\Registry;
use Dividebuy\Common\Utility\SessionHelper;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\Value;
use Magento\Framework\Model\Context;

class PaymentUpdate extends Value
{
  private SessionHelper $sessionHelper;

  public function __construct(
      SessionHelper $sessionHelper,
      Context $context,
      Registry $registry,
      TypeListInterface $cacheTypeList
  ) {
    $this->sessionHelper = $sessionHelper;

    parent::__construct($context, $registry, $sessionHelper->getStoreConfigHelper()->getScopeConfig(), $cacheTypeList);
  }

  public function beforeSave()
  {
    $configHelper = $this->sessionHelper->getStoreConfigHelper();
    $paymentButtonImage = $configHelper->getCheckoutButtonImage($configHelper->getStoreId());

    $this->sessionHelper->getBackendSession()->setPreviousPaymentButtonImage($paymentButtonImage);

    return parent::beforeSave();
  }
}
