<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Model\Sales;

use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Registry;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\UrlInterface;
use Magento\Sales\Model\Order;

class Redirect extends AbstractModel
{
  /**
   * Contains order details.
   */
  protected array $_orderDetails = [];

  protected Order $_orderModel;

  protected StoreConfigHelper $configHelper;

  protected Product $_productModel;

  public function __construct(
      Context $context,
      Registry $registry,
      Order $orderModel,
      Product $productModel,
      StoreConfigHelper $retailerConfigHelper
  ) {
    $this->_orderModel = $orderModel;
    $this->configHelper = $retailerConfigHelper;
    $this->_productModel = $productModel;

    parent::__construct($context, $registry);
  }

  /**
   * Used to get request which is to be send as a CURL request.
   *
   * @param  null  $orderId
   *
   * @return string
   *
   * @throws NoSuchEntityException
   */
  public function getRequest($orderId = null)
  {
    if ($orderId) {
      $order = $this->_orderModel->load($orderId);
      $storeId = $order->getStoreId();
      $this->_orderDetails['order_detail']['store_order_id'] = $order->getId();
      $this->_orderDetails['order_detail']['store_order_increment_id'] = $order->getIncrementId();
      $this->_orderDetails['order_detail']['order_status'] = $order->getStatus();
      $this->_orderDetails['order_detail']['order_state'] = $order->getState();
      if ($order->getDividebuySelectedInstalments()) {
        $this->_orderDetails['order_detail']['selected_instalment'] = (int) $order->getDividebuySelectedInstalments();
      }

      $tokenNumber = $this->configHelper->getStoreToken($storeId);
      $authNumber = $this->configHelper->getAuthenticationKey($storeId);
      if ($tokenNumber && $authNumber) {
        $this->_orderDetails['order_detail']['store_token'] = $tokenNumber;
        $this->_orderDetails['order_detail']['store_authentication'] = $authNumber;
      }

      $excludeKeys = [
          'entity_id',
          'customer_address_id',
          'quote_address_id',
          'region_id',
          'customer_id',
          'address_type',
      ];

      // Check the order items whether they contain product type other than virtual or downloadable
      $checkItemStatus = $this->checkItemsOfOrder($order);

      $orderBillingAddress = $order->getBillingAddress()->getData();

      // Setting billing address as shipping address if checkItemStatus is 0
      if ($checkItemStatus) {
        $orderShippingAddress = $order->getShippingAddress()->getData();
      } else {
        $orderShippingAddress = $order->getBillingAddress()->getData();
      }

      $orderBillingAddressFiltered = array_diff_key($orderBillingAddress, array_flip($excludeKeys));
      $orderShippingAddressFiltered = array_diff_key($orderShippingAddress, array_flip($excludeKeys));
      $addressDifference = array_diff($orderBillingAddressFiltered, $orderShippingAddressFiltered);
      // billing and shipping addresses are different
      if ($addressDifference) {
        $differentAddress = 1;
      } else {
        $differentAddress = 0;
      }

      $this->_orderDetails['order_detail']['logo_url'] = $this->configHelper->getRetailerLogoUrl();
      $this->_orderDetails['order_detail']['grand_total'] = (float) $this->_roundVal($order->getGrandTotal());
      $this->_orderDetails['order_detail']['subtotal'] = (float) $this->_roundVal($order->getSubtotal());
      $this->_orderDetails['order_detail']['subtotalInclVat'] = (float) $this->_roundVal($order->getSubtotalInclTax());
      $this->_orderDetails['order_detail']['discount'] = (float) $this->_roundVal($order->getDiscountAmount());
      $this->_orderDetails['order_detail']['discountApplied'] = $this->getDiscountVatStatus($storeId);
      $this->_orderDetails['order_detail']['shipping'] = (float) $this->_roundVal($order->getShippingAmount());
      $this->_orderDetails['order_detail']['shippingInclVat'] = (float) $this->_roundVal($order->getShippingInclTax());
      $this->_orderDetails['order_detail']['shipping_label'] = $order->getShippingDescription();
      $this->_orderDetails['order_detail']['is_shipping_default'] = $differentAddress;
      $this->_orderDetails['order_detail']['is_billing_default'] = $differentAddress;
      $this->_orderDetails['order_detail']['vat'] = (float) $this->_roundVal($order->getTaxAmount());
      $this->_orderDetails['product_details'] = $this->getProductDetails($order);
      $this->_orderDetails['shipping_address'] = $this->getAddress($order);
      $this->_orderDetails['billing_address'] = $this->getAddress($order, 'billing');

      return $this->configHelper->getJsonHelper()->jsonEncode($this->_orderDetails);
    }

    return '';
  }

  /**
   * Returns 1 if order contains products with type other than virtual and downloadable else 0.
   *
   * @param $order
   *
   * @return mixed
   */
  public function checkItemsOfOrder($order)
  {
    $flag = 0;
    $orderItems = $order->getallVisibleItems();
    $productType = [];

    // Creating array of order items product type
    foreach ($orderItems as $items) {
      $productType[] = $items->getData('product_type');
    }

    // Checking the occurrence of simple, configurable, bundle and grouped product type
    if (array_intersect($productType, ['simple', 'configurable', 'bundle', 'grouped'])) {
      $flag = 1;
    }

    return $flag;
  }

  /**
   * Decides whether vat is applied before or after Vat.
   *
   * @param  int  $storeId
   *
   * @return mixed
   */
  public function getDiscountVatStatus($storeId = null)
  {
    return $this->configHelper->getDiscountVatStatus($storeId);
  }

  /**
   * Used to get order details.
   *
   * @param  object  $order
   *
   * @return array
   *
   * @throws NoSuchEntityException
   */
  public function getProductDetails($order = null)
  {
    $productDetails = [];
    $productCount = 0;
    $globalTaxClass = $this->configHelper->getVatClass();
    // $taxFlag        = false;
    // if (is_numeric($globalTaxClass)) {
    //     $taxFlag = true;
    // }

    $orderItems = $order->getallVisibleItems();
    foreach ($orderItems as $item) {
      $productId = $item->getProductId();
      $productDetails[$productCount]['name'] = $item->getName();
      $productDetails[$productCount]['sku'] = $item->getSku();
      $productDetails[$productCount]['qty'] = $item->getQtyOrdered();
      $productDetails[$productCount]['price'] = (float) $item->getPrice();
      $productDetails[$productCount]['priceInclVat'] = (float) $item->getPriceInclTax();
      $productDetails[$productCount]['rowTotal'] = (float) $item->getRowTotal();
      $productDetails[$productCount]['rowTotalInclVat'] = (float) $item->getRowTotalInclTax();
      $productDetails[$productCount]['discount'] = (float) $item->getDiscountAmount();

      $productData = $this->_productModel->load($productId);
      if (!empty($productData->getShortDescription())) {
        $productDetails[$productCount]['short_description'] = $productData->getShortDescription();
      } elseif (!empty($productData->getDescription())) {
        $productDetails[$productCount]['short_description'] = $productData->getDescription();
      } else {
        $productDetails[$productCount]['short_description'] = $item->getName();
      }

      $productDetails[$productCount]['product_type'] = $productData->getTypeId();
      $productDetails[$productCount]['product_weight'] = $productData->getWeight();
      $productDetails[$productCount]['product_visibility'] = $productData->getVisibility();

      $productTaxClassOptionId = $productData->getDividebuyTaxClass();
      $productDetails[$productCount]['DivVat'] = 0;
      $productTaxClass = $item->getTaxPercent();

      if (!empty($productTaxClassOptionId)) {
        $productTaxClassOptionValue = $productData->getAttributeText('dividebuy_tax_class');
        if(!empty($productTaxClassOptionValue) && $productTaxClassOptionValue != "None"){
          $taxClass = explode('-', $productTaxClassOptionValue);
          $taxValue = trim(str_replace('%', '', $taxClass[1]));
          $productDetails[$productCount]['DivVat'] = $taxValue;
        }
      } elseif (!empty($globalTaxClass)) {
        $productDetails[$productCount]['DivVat'] = $globalTaxClass;
      } else {
        $productDetails[$productCount]['DivVat'] = $productTaxClass;
      }

      $productDetails[$productCount]['image_url'] = $this->getProductImageUrl($productData->getThumbnail());

      $productOptions = $item->getProductOptions();
      if (isset($productOptions['attributes_info'])) {
        $attribute = [];
        foreach ($productOptions['attributes_info'] as $option) {
          $attribute[$option['label']] = $option['value'];
        }
        $productDetails[$productCount]['product_options'] = $attribute;
      }
      ++$productCount;
    }

    return $productDetails;
  }

  /**
   * Used to retrieve product image URL.
   *
   * @param  string  $imageThumbName
   *
   * @return string
   *
   * @throws NoSuchEntityException
   */
  public function getProductImageUrl($imageThumbName)
  {
    $imageUrl = $this->configHelper->getBaseUrl(
        UrlInterface::URL_TYPE_MEDIA
    ).'/catalog/product'.$imageThumbName;
    $imageDirectory = $this->configHelper->getBaseUrl(
        UrlInterface::URL_TYPE_MEDIA
    ).'/dividebuy/product/images/'.$imageThumbName;

    if (!is_dir($imageDirectory)) {
      mkdir($imageDirectory);
    }

    if (!file_exists($imageDirectory) && file_exists($imageUrl)) {
      $imageObj = new Varien_Image($imageUrl);
      $imageObj->constrainOnly(true);
      $imageObj->keepAspectRatio(true);
      $imageObj->keepFrame(false);
      $imageObj->resize(135, 135);
      $imageObj->save($imageDirectory);
    }

    return $imageUrl;
  }

  /**
   * Used get address details of an order.
   *
   * @param  object  $order
   * @param  string  $type
   *
   * @return array
   */
  public function getAddress($order = null, $type = 'shipping')
  {
    if ($type === 'billing') {
      $address = $order->getBillingAddress();
    } else {
      // Check the order items whether they contain product type other than virtual or downloadable
      $checkItemStatus = $this->checkItemsOfOrder($order);

      // Setting billing address as shipping address if checkItemStatus is 0
      if ($checkItemStatus) {
        $address = $order->getShippingAddress();
      } else {
        $address = $order->getBillingAddress();
      }
    }

    $addressArray = [];

    $addressArray['first_name'] = $address->getFirstname();
    $addressArray['last_name'] = $address->getLastname();
    $addressArray['email'] = '';
    if ($address->getEmail() !== DivideBuy::DEFAULT_EMAIL) {
      $addressArray['email'] = $address->getEmail();
    }
    $addressArray['street'] = $address->getStreet();
    $addressArray['postcode'] = $address->getPostcode();
    $addressArray['region'] = $address->getRegion();
    $addressArray['city'] = $address->getCity();
    $addressArray['contact_number'] = $address->getTelephone();

    return $addressArray;
  }

  /**
   * Get order total details.
   *
   * @param  object  $order
   *
   * @return array
   */
  public function getOrderTotal($order = null): array
  {
    $totalArray = [];

    if ($order) {
      $totalArray['grand_total'] = $this->_roundVal((float) $order->getGrandTotal());
      $totalArray['subtotal'] = (float) $this->_roundVal($order->getSubtotal());
      $totalArray['discount'] = (float) $this->_roundVal($order->getDiscountAmount());
      $totalArray['shipping'] = (float) $this->_roundVal($order->getShippingAmount());
      $totalArray['vat'] = (float) $this->_roundVal($order->getTaxAmount());
    }

    return $totalArray;
  }

  /**
   * Used to round up the product price.
   *
   * @param  int  $value
   *
   * @return int
   */
  protected function _roundVal($value = null)
  {
    // return $this->_coreStoreModel->roundPrice($value);
    return $value;
  }
}
