<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Observer;

use Dividebuy\Common\BackendSession;
use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Magento\Backend\Model\Session;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem;
use Magento\Framework\Filesystem\Driver\File;

class PaymentObserver implements ObserverInterface
{
  /**
   * @var Session | BackendSession
   */
  protected Session $backendModelSession;

  protected File $fileManagement;

  protected Filesystem $fileSystem;

  protected StoreConfigHelper $configHelper;

  public function __construct(
      Session $backendModelSession,
      File $file,
      Filesystem $fileSystem,
      StoreConfigHelper $configHelper
  ) {
    $this->backendModelSession = $backendModelSession;
    $this->fileManagement = $file;
    $this->fileSystem = $fileSystem;
    $this->configHelper = $configHelper;
  }

  /**
   * Used to set hide divide buy field to 1.
   *
   * @param  EventObserver  $observer
   *
   * @throws FileSystemException
   */
  public function execute(EventObserver $observer)
  {
    $storeId = $this->configHelper->getStoreId();
    $mediaDir = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA)->getAbsolutePath().DivideBuy::DIVIDEBUY_MEDIA_DIR;

    $paymentButtonImage = $this->configHelper->getCheckoutButtonImage($storeId);
    $previousImg = $this->backendModelSession->getPreviousPaymentButtonImage();

    if (!empty($previousImg) && $previousImg !== $paymentButtonImage) {
      $fullPath = $mediaDir.$previousImg;

      if ($this->fileManagement->isExists($fullPath)) {
        $this->fileManagement->deleteFile($fullPath);
      }
    }

    $this->backendModelSession->unsPreviousPaymentButtonImage();
  }
}
