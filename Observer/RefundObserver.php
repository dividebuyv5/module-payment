<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Observer;

use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Constants\DivideBuy;
use Magento\Framework\DataObject;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Dividebuy\Common\EventObserver;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\Order;

class RefundObserver implements ObserverInterface
{
  protected StoreConfigHelper $configHelper;

  protected Order $orderModel;

  protected ApiHelper $apiHelper;

  protected RequestInterface $request;

  public function __construct(
      StoreConfigHelper $config,
      Order $orderModel,
      ApiHelper $apiHelper,
      RequestInterface $request
  ) {
    $this->configHelper = $config;
    $this->orderModel = $orderModel;
    $this->apiHelper = $apiHelper;
    $this->request = $request;
  }

  /**
   * Used to set hide divide buy field to 1.
   *
   * @param  Observer|EventObserver  $observer
   *
   * @return void
   */
  public function execute(Observer $observer)
  {
    $params = $this->request->getParams();
    $postData = new DataObject($params ?? []);

    $request = [];
    $orderProducts = $observer->getCreditmemo()->getItems();
    $i = 0;

    $orderId = $observer->getCreditmemo()->getOrderId();
    $order = $this->orderModel->load($orderId);
    $paymentMethod = $order->getPayment()->getMethod();

    // Condition to check whether the order is dividebuy or not
    if ($paymentMethod === DivideBuy::DIVIDEBUY_PAYMENT_CODE) {
      $creditMemoData = $observer->getCreditmemo()->getData();
      $creditMemoDetails = new DataObject($creditMemoData ?? []);

      // Refund Details
      foreach ($orderProducts as $product) {
        if ($product->getData('base_price') > 0) {
          $request['product'][$i]['sku'] = $product->getData('sku');
          $request['product'][$i]['productName'] = $product->getData('name');
          $request['product'][$i]['qty'] = $product->getData('qty');
          $request['product'][$i]['rowTotal'] = $product->getData('row_total');
          $request['product'][$i]['rowInclTotal'] = $product->getData('row_total_incl_tax');
          $request['product'][$i]['rowInclTotal'] = $product->getData('row_total_incl_tax');
        }
      }
      if ($creditMemoDetails->getDataByKey('discount_amount')) {
        $request['discountAmount'] = str_replace('-', '', $creditMemoDetails->getDataByKey('discount_amount'));
      }

      $creditMemo = new DataObject($postData->getDataByKey('creditmemo') ?? []);
      $request['totalRefund'] = $observer->getCreditmemo()->getBaseGrandTotal();
      $request['orderId'] = $observer->getCreditmemo()->getOrderId();
      $request['reason'] = $creditMemo->getDataByKey('comment_text');
      $request['adjustmentRefund'] = $creditMemo->getDataByKey('adjustment_positive');
      $request['adjustmentFee'] = $creditMemo->getDataByKey('adjustment_negative');
      $request['taxAmount'] = $creditMemoDetails->getDataByKey('tax_amount');
      $request['reason'] = $creditMemo->getDataByKey('comment_text');
      $request['shippingCostAmount'] = $creditMemoDetails->getDataByKey('shipping_amount');
      $request['shippingTaxAmount'] = $creditMemoDetails->getDataByKey('shipping_incl_tax');

      $request['refundType'] = 'FULL';
      if ($order->getData('base_grand_total') != $request['totalRefund']) {
        $request['refundType'] = 'PARTIAL';
      }

      // Retailer Details
      $storeId = $order->getStoreId();

      $request['retailer']['retailerId'] = $this->configHelper->getRetailerId($storeId);
      $request['retailer']['storeAuthentication'] = $this->configHelper->getAuthenticationKey($storeId);
      $request['retailer']['storeToken'] = $this->configHelper->getStoreToken($storeId);

      $this->apiHelper->getSdkApi('', $storeId)->refundOrder($request);
    }
  }
}
