<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Observer;

use Dividebuy\Common\CheckoutSession;
use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\EventObserver;
use Exception;
use Magento\Checkout\Model\Session;
use Magento\Framework\Event\Observer as MagentoObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\QuoteFactory;

class OrderSaveAfter implements ObserverInterface
{
  protected QuoteFactory $_quoteLoader;

  /**
   * @var Session | CheckoutSession
   */
  protected Session $_checkoutSession;

  public function __construct(QuoteFactory $quoteFactory, Session $checkoutSession)
  {
    $this->_quoteLoader = $quoteFactory;
    $this->_checkoutSession = $checkoutSession;
  }

  /**
   * Used to set hide divide buy field to 1.
   *
   * @param  MagentoObserver | EventObserver  $observer
   *
   * @throws Exception
   */
  public function execute(MagentoObserver $observer)
  {
    $order = $observer->getEvent()->getOrder();
    $quoteId = $order->getQuoteId();
    $quoteObj = $this->_quoteLoader->create()->load($quoteId);

    $paymentMethod = $quoteObj->getPayment()->getMethod();

    if ($paymentMethod === DivideBuy::DIVIDEBUY_PAYMENT_CODE) {
      $order->setDividebuySelectedInstalments($this->_checkoutSession->getSelectedInstalment());
      $order->save();
    }
  }
}
