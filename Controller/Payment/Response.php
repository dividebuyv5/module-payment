<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Payment;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\SessionHelper;
use Dividebuy\Payment\Helper\Data as PaymentData;
use Exception;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Magento\Store\Model\Store;

class Response extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  private Order $orderModel;
  private ApiHelper $apiHelper;
  private Store $storeModel;
  private PaymentData $paymentHelper;
  private SessionHelper $sessionHelper;

  public function __construct(
      Context $context,
      ApiHelper $apiHelper,
      PaymentData $responseHelper,
      Order $orderModel,
      Store $storeModel,
      SessionHelper $sessionHelper
  ) {
    $this->orderModel = $orderModel;
    $this->apiHelper = $apiHelper;
    $this->paymentHelper = $responseHelper;
    $this->storeModel = $storeModel;
    $this->sessionHelper = $sessionHelper;

    parent::__construct($context);
  }

  /**
   * Used to cancel or success the order.
   *
   * @return ResponseInterface|ResultInterface
   *
   * @throws Exception
   * @throws LocalizedException
   */
  public function execute()
  {
    $requestData = base64_decode((string) $this->getRequest()->getParam('splashKey'));
    if (empty($requestData)) {
      return $this->_redirect('checkout/cart/index', ['_secure' => true]);
    }

    $requestArray = explode(':', $requestData);

    $orderId = $requestArray[0] ?? '';
    $method = $requestArray[1] ?? '';
    $storeToken = $requestArray[2] ?? '';
    $storeAuthentication = $requestArray[3] ?? '';
    $storeCode = $requestArray[4] ?? '';
    $store = $this->storeModel->load($storeCode, 'code');
    $storeId = $store->getStoreId();

    //verify retailer token and authentication
    $failedAuthentication = $this->sessionHelper->getStoreConfigHelper()
        ->isFailedAuth($storeToken, $storeAuthentication, $storeId);

    if ($failedAuthentication) {
      $this->messageManager->addError(__('Authentication failed.'));

      return $this->_redirect('checkout/cart');
    }

    $this->sessionHelper->getCheckoutSession()->setDividebuyOrderId($orderId);
    $order = $this->orderModel->load($orderId);
    $paymentMethod = $order->getPayment()->getMethod();

    if ($paymentMethod !== DivideBuy::DIVIDEBUY_PAYMENT_CODE) {
      $this->messageManager->addError(__('Non-DivideBuy Order'));

      return $this->_redirect('checkout/cart');
    }

    if (empty($orderId)) {
      return $this->_redirect('checkout/cart');
    }

    if ($method === 'cancel') {
      return $this->handleCancel($orderId);
    }

    if ($method === 'success') {
      return $this->handleSuccessfulOrder($order);
    }

    return $this->_redirect('checkout/cart');
  }

  private function handleSuccessfulOrder($order): ResponseInterface
  {
      $storeId = $this->sessionHelper->getStoreConfigHelper()->getStoreId();
      $sdk = $this->apiHelper->getSdkApi('', $storeId);
      //@todo check the response is correct
      $responseData = $sdk->getUserOrder($order->getId());

      if (!empty($responseData)) {
        $responseData = $this->apiHelper->encode($responseData);
        $orderDetails = $this->apiHelper->decode($responseData);
        // Calling complete order for setting latest order data
        $this->paymentHelper->completeOrder($order, $orderDetails);
        $sdk->syncRetailerOrder((int) $order->getId());
      }
      return $this->_redirect('dividebuy/payment/success');

  }

  /**
   * Used to cancel the order.
   *
   * @param $orderId
   *
   * @return ResponseInterface
   *
   * @throws Exception
   */
  private function handleCancel($orderId): ResponseInterface
  {
    $order = $this->orderModel->load($orderId);
    $order->cancel()->setState(Order::STATE_CANCELED)->save();

    return $this->_forward('createcart', null, null, ['order_id' => $orderId]);
  }
}
