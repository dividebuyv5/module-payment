<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Payment;

use Dividebuy\Common\CheckoutSession;
use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Payment\Helper\Data;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Result\Page;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\Store;

class Success extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected PageFactory $resultPageFactory;

  /** @var Session | CheckoutSession */
  protected Session $checkoutSession;

  protected Data $paymentHelper;

  protected ApiHelper $apiHelper;

  private Store $storeManager;

  public function __construct(
      Context $context,
      Store $storeManager,
      PageFactory $resultPageFactory,
      Session $checkoutSession,
      Data $paymentHelper,
      ApiHelper $apiHelper
  ) {
    $this->storeManager = $storeManager;
    $this->resultPageFactory = $resultPageFactory;
    $this->checkoutSession = $checkoutSession;
    $this->paymentHelper = $paymentHelper;
    $this->apiHelper = $apiHelper;

    parent::__construct($context);
  }

  /**
   * Used to redirect user to the success page.
   *
   * @return Page|ResponseInterface|ResultInterface
   *
   * @throws LocalizedException
   */
  public function execute()
  {
    //Checking for phone order session and redirecting to portal.
    $phoneOrderTokenSession = $this->checkoutSession->getDividebuyPhoneOrderToken();
    if ($phoneOrderTokenSession) {
      // Getting store ID.
      $store = $this->storeManager->load('default', 'code');
      $storeId = $store->getId();
      $portalUrl = $this->apiHelper->getPortalUrl($storeId);
      $this->_redirect($portalUrl);
      $this->checkoutSession->unsDividebuyPhoneOrderToken();
    }

    if ($this->checkoutSession->getDividebuyOrderId()) {
      $this->paymentHelper->generateInvoice($this->checkoutSession->getDividebuyOrderId());

      return $this->resultPageFactory->create();
    }

    return $this->_redirect('checkout/cart');
  }
}
