<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Payment;

use Dividebuy\Common\CheckoutSession;
use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Magento\Checkout\Model\Cart;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order;
use Throwable;

class CreateCart extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  /**
   * @var Session | CheckoutSession
   */
  protected Session $checkoutSession;

  protected Order $orderModel;

  protected Cart $cart;

  public function __construct(
      Context $context,
      Session $checkoutSession,
      Order $orderModel,
      Cart $cart
  ) {
    $this->checkoutSession = $checkoutSession;
    $this->orderModel = $orderModel;
    $this->cart = $cart;

    parent::__construct($context);
  }

  /**
   * Used to regenerate cart after order is cancelled from divide buy.
   *
   * @return ResponseInterface|ResultInterface
   */
  public function execute()
  {
    try {
      $orderId = $this->getRequest()->getParam('order_id');
      $order = $this->orderModel->load($orderId);
      $items = $order->getItemsCollection();

      foreach ($items as $item) {
        $this->cart->addOrderItem($item);
      }

      $this->cart->save();

      // Load current quote.
      $quote = $this->checkoutSession->getQuote();
      $this->updateQuoteShippingAddress($quote);

      $this->messageManager->addErrorMessage(__('Your DivideBuy order has been cancelled for you to amend your basket.'));
    } catch (NoSuchEntityException | LocalizedException $exception) {
      if ($this->checkoutSession->getUseNotice(true)) {
        $this->messageManager->addNoticeMessage($exception->getMessage());
      } else {
        $this->messageManager->addErrorMessage($exception->getMessage());
      }
    } catch (Throwable $exception) {
      $this->messageManager->addExceptionMessage(
          $exception,
          __('Cannot add the item to shopping cart.')
      );
    }
    return $this->_redirect('checkout/cart');
  }

  /**
   * Function to set default address to null.
   *
   * @param $quote
   */
  protected function updateQuoteShippingAddress($quote)
  {
    // Update quote customer email.
    if ($quote->getCustomerEmail() === DivideBuy::DEFAULT_EMAIL) {
      $quote->setCustomerEmail('');
    }

    // Set shipping address fields to null.
    $quoteShippingAddress = $quote->getShippingAddress();
    if ($quoteShippingAddress->getFirstName() === DivideBuy::DEFAULT_FIRST_NAME) {
      $quoteShippingAddress->setFirstname('');
    }
    if ($quoteShippingAddress->getLastName() === DivideBuy::DEFAULT_LAST_NAME) {
      $quoteShippingAddress->setLastname('');
    }
    if ($quoteShippingAddress->getEmail() === DivideBuy::DEFAULT_EMAIL) {
      $quoteShippingAddress->setEmail('');
    }
    if ($quoteShippingAddress->getStreet() && $quoteShippingAddress->getStreet() === DivideBuy::DEFAULT_STREET) {
      $quoteShippingAddress->setStreet('');
    }
    if ($quoteShippingAddress->getTelephone() === DivideBuy::DEFAULT_TELEPHONE) {
      $quoteShippingAddress->setTelephone('');
    }
    if ($quoteShippingAddress->getCity() === DivideBuy::DEFAULT_CITY) {
      $quoteShippingAddress->setCity('');
    }
    if ($quoteShippingAddress->getRegion() === DivideBuy::DEFAULT_REGION) {
      $quoteShippingAddress->setRegion('');
    }
    $quote->save();
  }
}
