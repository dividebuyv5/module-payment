<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Exception\InvalidParameterException;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\ResponseHelper;
use Dividebuy\Payment\Helper\Data;
use Dividebuy\Payment\Helper\Data as PaymentData;
use DivideBuySdk\Request\Incoming\ActivatePosSystemRequest;
use Exception;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Store\Model\Store;
use Magento\Store\Model\Store as StoreModel;

class ActivatePosSystem extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  private PaymentData $paymentHelper;
  private StoreModel $storeModel;
  private ProductRepositoryInterface $productRepository;
  private ResponseHelper $responseHelper;
  private ApiHelper $apiHelper;

  /**
   * Constructor for activate POS API.
   *
   * @param  Context  $context
   * @param  PaymentData  $paymentHelper
   * @param  StoreModel  $storeModel
   * @param  ProductRepositoryInterface  $productRepository
   * @param  ResponseHelper  $responseHelper
   * @param  ApiHelper  $apiHelper
   */
  public function __construct(
      Context $context,
      Data $paymentHelper,
      Store $storeModel,
      ProductRepositoryInterface $productRepository,
      ResponseHelper $responseHelper,
      ApiHelper $apiHelper
  ) {
    $this->paymentHelper = $paymentHelper;
    $this->productRepository = $productRepository;
    $this->storeModel = $storeModel;
    $this->responseHelper = $responseHelper;
    $this->apiHelper = $apiHelper;

    parent::__construct($context);
  }

  /**
   * This function will be executed once Activate POS API will be called from DivideBuy.
   *
   * @return ResponseInterface|ResultInterface
   *
   * @throws Exception
   */
  public function execute()
  {
    $api = $this->apiHelper->getSdkApi();
    $result = $api->activatePosSystem([$this, 'handleActivatePosRequest']);

    return $this->responseHelper->sendJsonResponse($result);
  }

  public function handleActivatePosRequest(ActivatePosSystemRequest $request): bool
  {
    $storeCode = $request->getRetailerStoreCode();
    $store = $this->storeModel->load($storeCode, 'code');
    $storeId = $store->getStoreId();
    $websiteId = $store->getWebsiteId();
    $activatePos = $request->getActivatePos();

    // Checking if POS is activated.
    if ($activatePos === 1) {
      // Creating POS product and activating shipping.
      $posProduct = $this->paymentHelper->createPosProduct($websiteId);
      $this->paymentHelper->activateDeactivatePosShipping(1, $storeId);
      if ($posProduct) {
        return true;
      }
    } elseif ($activatePos === 0) {
      // Disabling product and custom shipping when POS is deactivated.
      $product = $this->productRepository->get(DivideBuy::DIVIDEBUY_POS_PRODUCT);
      $product->setStatus(2);
      $product->save();
      $this->paymentHelper->activateDeactivatePosShipping(0, $storeId);

      return true;
    } elseif ($activatePos === null) {
      throw new InvalidParameterException('An error occurred. We are unable to activate POS on Retailer\'s end', 422);
    }

    throw new InvalidParameterException(
        'An error occurred. We are unable to create the product on Retailer\'s end',
        422
    );
  }
}
