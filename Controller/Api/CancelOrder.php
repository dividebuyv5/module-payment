<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\OrderHelper;
use Dividebuy\Common\Utility\ResponseHelper;
use DivideBuySdk\Request\Incoming\CancelOrderRequest;
use InvalidArgumentException;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Model\Order;

class CancelOrder extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  private ApiHelper $apiHelper;

  private OrderHelper $orderHelper;

  private ResponseHelper $responseHelper;

  /**
     * @var \Magento\Framework\Json\Helper\Data
     */
  protected $_jsonHelper;

  /**
     * Magento\Store\Model\Store
     */
    protected $_storeModel;

  public function __construct(
      Context $context,
      OrderHelper $orderHelper,
      ApiHelper $apiHelper,
      ResponseHelper $responseHelper,
      \Magento\Framework\Json\Helper\Data $jsonHelper,
      \Magento\Store\Model\Store $storeModel
  ) {
    $this->apiHelper = $apiHelper;
    $this->orderHelper = $orderHelper;
    $this->responseHelper = $responseHelper;
    $this->_jsonHelper  = $jsonHelper;
    $this->_storeModel  = $storeModel;

    parent::__construct($context);
  }

  /**
   * Used to cancel an order.
   *
   * @return ResponseInterface|ResultInterface
   */
  public function execute()
  {
    $post     = trim(file_get_contents("php://input"));
    $postData = $this->_jsonHelper->jsonDecode($post);
    $storeToken = $postData['store_token'];
    $storeAuthentication = $postData['store_authentication'];
    $storeCode = $postData['retailer_store_code'];
    $store = $this->_storeModel->load($storeCode);
    $storeId = $store->getStoreId();
    $api = $this->apiHelper->getSdkApi($env = '', $storeId);
    $result = $api->cancelOrder([$this, 'handleCancelRequest']);

    return $this->responseHelper->sendJsonResponse($result);
  }

  public function handleCancelRequest(CancelOrderRequest $request): array
  {
    $storeOrderId = $request->getStoreOrderId();
    $store_code = $request->getRetailerStoreCode();
    $order = $this->orderHelper->loadOrderById($storeOrderId);
    $order_id = $order->getId();
    $validate_store = $this->orderHelper->validateOrderStore($order_id,$store_code);

    if($order && $order->getId()) {
      $result = $this->doCancelOrder($order);
      $deleteUserOrder = $request->getParameterValue('delete_user_order');

      if ($deleteUserOrder === 1) {
        $this->responseHelper->getRegistry()->register('isSecureArea', true);
        $this->responseHelper->debugResponse($request->getContent()->getArrayCopy());
        $order->delete();
        $this->responseHelper->getRegistry()->unregister('isSecureArea');
        $result = ['error' => 0, 'success' => 1, 'status' => 'ok'];
      }

      return $result;
    }
    throw new InvalidArgumentException('order not found', 404);
  }

  /**
   * To cancel an order.
   *
   * @param $order mixed
   *
   * @return array of the order with cancelled
   */
  protected function doCancelOrder($order): array
  {
    $paymentMethod = $order->getPayment()->getMethod();
    if ($order->getState() === Order::STATE_CANCELED) {
      throw new InvalidArgumentException('Order is already cancelled', 405);
    }
    if ($paymentMethod !== DivideBuy::DIVIDEBUY_PAYMENT_CODE) {
      throw new InvalidArgumentException('Non-DivideBuy order', 401);
    }
    $order->cancel()
        ->setState(Order::STATE_CANCELED, true, 'Order cancelled from dividebuy')
        ->setStatus(Order::STATE_CANCELED, true, 'Order cancelled from dividebuy')
        ->save();

    return [
        'error' => 0,
        'success' => 1,
        'status' => 'ok',
        'order_id' => $order->getId(),
    ];
  }
}
