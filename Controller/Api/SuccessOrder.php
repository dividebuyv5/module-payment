<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\OrderHelper;
use Dividebuy\Common\Utility\ResponseHelper;
use Dividebuy\Common\DivideBuyOrderInterface;
use DivideBuySdk\Request\Incoming\OrderSuccessRequest;
use InvalidArgumentException;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Model\Order;

class SuccessOrder extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  private Order $orderModel;

  private ApiHelper $apiHelper;

  private OrderHelper $orderHelper;

  private ResponseHelper $responseHelper;

  /**
     * @var \Magento\Framework\Json\Helper\Data
     */
  protected $_jsonHelper;

  /**
     * Magento\Store\Model\Store
     */
    protected $_storeModel;

  public function __construct(
      Context $context,
      Order $cartHelper,
      ApiHelper $apiHelper,
      OrderHelper $orderHelper,
      ResponseHelper $responseHelper,
      \Magento\Framework\Json\Helper\Data $jsonHelper,
      \Magento\Store\Model\Store $storeModel
  ) {
    $this->orderModel = $cartHelper;
    $this->apiHelper = $apiHelper;
    $this->orderHelper = $orderHelper;
    $this->responseHelper = $responseHelper;
    $this->_jsonHelper  = $jsonHelper;
    $this->_storeModel  = $storeModel;

    parent::__construct($context);
  }

  /**
   * Process the success order.
   *
   * @return ResponseInterface|ResultInterface
   */
  public function execute()
  {
    $post     = trim(file_get_contents("php://input"));
    $postData = $this->_jsonHelper->jsonDecode($post);
    $storeToken = $postData['store_token'];
    $storeAuthentication = $postData['store_authentication'];
    $storeCode = $postData['retailer_store_code'];
    $store = $this->_storeModel->load($storeCode);
    $storeId = $store->getStoreId();
    $api = $this->apiHelper->getSdkApi($env = '', $storeId);
    $storeOrderId = $postData['store_order_id'];
    $order = $this->orderModel->load($storeOrderId);
    $order_id = $order->getId();
    $order_store_code = $order->getStore()->getStoreId();
    if($order_store_code != $storeCode){
        $result = [
        'error' => 1,
        'success'=> 0,
        'message'=> "Order not found for entered store code",
        'status'=> 404,
      ];
      return $this->responseHelper->sendJsonResponse($result);
    }
    $result = $api->successOrder([$this, 'handleSuccessOrderRequest']);

    return $this->responseHelper->sendJsonResponse($result);
  }

  public function handleSuccessOrderRequest(OrderSuccessRequest $request)
  {
    $orderId = $request->getStoreOrderId();
    $address = $request->getAddress();
    $customerEmail = $request->getCustomerEmail();
    $orderReferenceId = $request->getOrderReferenceId();
    $isPhoneOrderEnabled = $request->isPhoneOrderEnabled();

    $street1 = $address->getHouseNumber().' '.$address->getHouseName().', '.$address->getStreet();
    $street1 = ltrim($street1, ', ');
    $street2 = $address->getAddress2();
    $street = [
        '0' => $street1,
        '1' => $street2,
    ];

    $userAddress = [
        'prefix' => $address->getPrefix(),
        'firstname' => $address['first_name'],
        'lastname' => $address['last_name'],
        'street' => $street,
        'postcode' => $address->getPostCode(),
        'region' => $address->getRegion(),
        'city' => $address->getCity(),
        'email' => $customerEmail,
        'telephone' => $address->getContactNumber(),
    ];

    /** @var DivideBuyOrderInterface $order */
    /** @var DivideBuyOrderInterface $orderEmail */
    $order = $this->orderModel->load($orderId);
    $result = $this->orderHelper->setOrderData($order, $userAddress);
    $this->responseHelper->debugResponse($result, 'OrderSuccess');

    if ($order->getId()) {
      // verify the order is dividebuy or not
      $isDivideBuyOrder = $this->orderHelper->validateDividebuyOrder($order);
      if (!$isDivideBuyOrder) {
        throw new InvalidArgumentException('Non-Dividebuy order.', 402);
      }

      // Sending order email only if order is DivideBuy and it is visible in order grid.
        $orderEmail = $this->orderModel->loadByIncrementId($order->getIncrementId());

        // Checking if order is placed via phone order.
        if ($isPhoneOrderEnabled) {
          $orderEmail->addStatusHistoryComment(
              'DivideBuy order authenticated via phone order. Transaction ID : "'.$orderReferenceId.'"',
              Order::STATE_PROCESSING
          );
        } else {
          $orderEmail->addStatusHistoryComment(
              'DivideBuy order authenticated. Transaction ID : "'.$orderReferenceId.'"',
              Order::STATE_PROCESSING
          );
        }
        $orderEmail->save();
    } else {
      throw new InvalidArgumentException('order not found', 404);
    }
  }
}
