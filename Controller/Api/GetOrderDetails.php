<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\OrderHelper;
use Dividebuy\Common\Utility\ResponseHelper;
use Dividebuy\Payment\Model\Sales\Redirect;
use DivideBuySdk\Request\Incoming\GetOrderDetailsRequest;
use InvalidArgumentException;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;

class GetOrderDetails extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected Redirect $paymentRedirectModel;

  private ApiHelper $apiHelper;

  private OrderHelper $orderHelper;

  private ResponseHelper $responseHelper;

  /**
     * @var \Magento\Framework\Json\Helper\Data
     */
  protected $_jsonHelper;

  /**
     * Magento\Store\Model\Store
     */
    protected $_storeModel;

  public function __construct(
      Context $context,
      ApiHelper $apiHelper,
      Redirect $responseRedirectModel,
      OrderHelper $orderHelper,
      ResponseHelper $responseHelper,
      \Magento\Framework\Json\Helper\Data $jsonHelper,
      \Magento\Store\Model\Store $storeModel
  ) {
    $this->paymentRedirectModel = $responseRedirectModel;
    $this->apiHelper = $apiHelper;
    $this->orderHelper = $orderHelper;
    $this->responseHelper = $responseHelper;
    $this->_jsonHelper  = $jsonHelper;
    $this->_storeModel  = $storeModel;

    parent::__construct($context);
  }

  /**
   * Used to get Order details.
   */
  public function execute()
  {
    $post     = trim(file_get_contents("php://input"));
    $postData = $this->_jsonHelper->jsonDecode($post);
    $storeToken = $postData['storeToken'];
    $storeAuthentication = $postData['storeAuthentication'];
    $storeCode = $postData['retailerStoreCode'];
    $store = $this->_storeModel->load($storeCode);
    $storeId = $store->getStoreId();
    $api = $this->apiHelper->getSdkApi($env = '', $storeId);
    $response = $api->getOrderDetails([$this, 'handleGetOrderDetailsRequest']);
    return $this->responseHelper->sendJsonResponse($response);
  }

  public function handleGetOrderDetailsRequest(GetOrderDetailsRequest $request)
  {
    $orderId = $request->getOrderId();
    $store_code = $request->getRetailerStoreCode();
    $order = $this->orderHelper->loadOrderById($orderId);
    $order_id = $order->getId();
    $validate_store = $this->orderHelper->validateOrderStore($order_id,$store_code);

    $isDivideBuyOrder = $this->orderHelper->validateDividebuyOrder($order);
    if (!$isDivideBuyOrder) {
      throw new InvalidArgumentException('Non-Dividebuy order.', 402);
    }
    $result = $this->paymentRedirectModel->getRequest($orderId);

    return $this->apiHelper->decode($result);
  }
}
