<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\DivideBuyRequest;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\ResponseHelper;
use Exception;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Model\Order;

class DeleteOrder extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected Order $orderModel;
  private ResponseHelper $responseHelper;

  public function __construct(Context $context, Order $orderModel, ResponseHelper $responseHelper)
  {
    $this->orderModel = $orderModel;
    $this->responseHelper = $responseHelper;

    parent::__construct($context);
  }

  /**
   * Save courier list for DivideBuy Shipping.
   *
   * @return ResponseInterface|ResultInterface
   *
   * @throws Exception
   */
  public function execute()
  {
    $post = $this->responseHelper->getRequestStreamData();
    $request = new DivideBuyRequest($post);
    $storeOrderId = $request->getParameterValue('store_order_id');
    $order = $this->orderModel->load($storeOrderId);
    $retailer_store_code = $request->getParameterValue('retailer_store_code');
    $order_id = $order->getId();
    $order_store_code = $order->getStore()->getStoreId();
    if($order_store_code != $retailer_store_code){
        $result = [
        'error' => 1,
        'success'=> 0,
        'message'=> "Order not found for entered store code",
        'status'=> 404,
      ];
      return $this->responseHelper->sendJsonResponse($result);
    }
    $flagToDeleteOrders = $this->responseHelper->getConfigHelper()->getFlagToDeleteOrder();


    $order->cancel()
        ->setState(Order::STATE_CANCELED)
        ->setStatus(Order::STATE_CANCELED);

    $order->save();

    if ($flagToDeleteOrders) {
      $this->responseHelper->getRegistry()->register('isSecureArea', true);
      $order->delete();
      $this->responseHelper->getRegistry()->unregister('isSecureArea');
    }

    $result = [
        'error' => 0,
        'success' => 1,
        'status' => 'ok',
    ];

    return $this->responseHelper->sendJsonResponse($result);
  }
}
