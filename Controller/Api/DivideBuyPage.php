<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\DivideBuyRequest;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\ResponseHelper;
use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Cms\Model\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Store\Model\Store;

class DivideBuyPage extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected PageFactory $pageFactory;

  protected PageRepositoryInterface $pageRepository;

  protected Store $storeModel;

  private ResponseHelper $responseHelper;

  public function __construct(
      Context $context,
      PageFactory $pageFactory,
      PageRepositoryInterface $pageRepository,
      Store $storeModel,
      ResponseHelper $responseHelper
  ) {
    $this->pageFactory = $pageFactory;
    $this->pageRepository = $pageRepository;
    $this->storeModel = $storeModel;
    $this->responseHelper = $responseHelper;

    parent::__construct($context);
  }

  /**
   * Used to create dividebuy page.
   *
   * @return mixed|ResponseInterface|ResultInterface
   *
   * @throws LocalizedException
   */
  public function execute()
  {
    $post = $this->responseHelper->getRequestStreamData();
    $request = new DivideBuyRequest($post);
    $postData = $request->getContent()->getArrayCopy();

    //if data is blank sending error response
    if (empty($postData)) {
      $result = [
          'error' => 1,
          'success' => 0,
          'message' => 'There is a problem in retrieving data',
          'status' => '406',
      ];
      // For error log code start
      $this->responseHelper->debugResponse($result, 'Guarantor api there is a problem in retrieving the data');

      return $this->responseHelper->sendJsonResponse($result);
    }

    //Request authentication
    $storeToken = $postData['store_token'];
    $storeAuthentication = $postData['store_authentication'];
    $storeCode = $postData['retailer_store_code'];
    $store = $this->storeModel->load($storeCode, 'code');

    $authenticationStatus = $this->responseHelper->getConfigHelper()
        ->isFailedAuth($storeToken, $storeAuthentication, $store->getStoreId());

    if ($authenticationStatus) {
      return $this->responseHelper->sendJsonResponse($authenticationStatus);
    }

    //Creating cms page and if identifier is there than it will update the page
    $identifier = 'dividebuy-interestfree-credit';
    //getting the store id
    $storeId = $this->responseHelper->getConfigHelper()->getStoreId();

    $cmsPage = $this->pageFactory->create();
    $cmsPage = $cmsPage->load($identifier);
    $cmsPage->setIdentifier($identifier)
        ->setTitle($postData['title'])
        ->setContentHeading('')
        ->setContent($postData['content'])
        ->setPageLayout('1column')
        ->setData('stores', [$storeId]);

    $this->pageRepository->save($cmsPage);

    //Giving the response back
    $result = ['error' => 0, 'success' => 1, 'message' => 'ok', 'status' => '200'];
    $this->responseHelper->debugResponse(
        $result,
        'Guarantor api there is a problem in retrieving the data',
        null,
        ['post' => $post]
    );

    return $this->responseHelper->sendJsonResponse($result);
  }
}
