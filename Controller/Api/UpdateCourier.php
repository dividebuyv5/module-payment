<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\ResponseHelper;
use DivideBuySdk\Request\Incoming\UpdateCourierRequest;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class UpdateCourier extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  private ApiHelper $apiHelper;

  private ResponseHelper $responseHelper;

  /**
     * @var \Magento\Framework\Serialize\Serializer\Serialize
     */
    private $serialize;

  public function __construct(
    Context $context, 
    ApiHelper $apiHelper, 
    ResponseHelper $responseHelper,
    \Magento\Framework\Serialize\Serializer\Serialize $serialize
  )
  {
    $this->apiHelper = $apiHelper;
    $this->responseHelper = $responseHelper;
    $this->serialize = $serialize;

    parent::__construct($context);
  }

  /**
   * Save courier list for DivideBuy Shipping.
   *
   * @return ResponseInterface|ResultInterface
   */
  public function execute()
  {
    $result = $this->apiHelper->getSdkApi()->updateCouriers([$this, 'handleUpdateRequest']);

    return $this->responseHelper->sendJsonResponse($result);
  }

  public function handleUpdateRequest(UpdateCourierRequest $request): array
  {
    $value = $this->serialize->serialize($request->getCouriers());
    $storeId = 0;
    $scope = 'default';
    $this->responseHelper->getConfigHelper()->saveConfig(DivideBuy::DIVIDEBUY_COURIERS, $value, $scope, $storeId);

    return ['error' => 0, 'success' => 1, 'status' => 'ok'];
  }
}
