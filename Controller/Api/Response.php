<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Constants\XmlFilePaths;
use Dividebuy\Common\DivideBuyRequest;
use Dividebuy\Common\Exception\InvalidParameterException;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\ResponseHelper;
use Dividebuy\Payment\Helper\Data as PaymentData;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Model\Order;
use Magento\Store\Model\Store;
use Throwable;

class Response extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  private StoreConfigHelper $configHelper;

  protected Order $orderModel;

  protected PaymentData $paymentHelper;

  protected Store $storeModel;

  protected ResponseHelper $responseHelper;

  private ApiHelper $apiHelper;

  public function __construct(
      Context $context,
      StoreConfigHelper $configHelper,
      PaymentData $paymentHelper,
      Order $orderModel,
      Store $storeModel,
      ResponseHelper $responseHelper,
      ApiHelper $apiHelper
  ) {
    $this->configHelper = $configHelper;
    $this->paymentHelper = $paymentHelper;
    $this->orderModel = $orderModel;
    $this->storeModel = $storeModel;
    $this->responseHelper = $responseHelper;
    $this->apiHelper = $apiHelper;

    parent::__construct($context);
  }

  /**
   * Used to redirect to another action according to the request.
   *
   * @return ResponseInterface|ResultInterface
   */
  public function execute()
  {
    $post = $this->responseHelper->getRequestStreamData();
    $request = new DivideBuyRequest($post);

    try {
      $postData = $this->validateRequest($request);

      $storeCode = $request->getRetailerStoreCode();
      $store = $this->storeModel->load($storeCode);
      $storeId = $store->getStoreId();

      $allowedIps = $request->getParameterValue('allowedIps');
      $this->checkAllowedIps($postData, $allowedIps, $storeId);

      $deactivated = (int) $request->getParameterValue('is_deactivated');

      // Code Activate the extension if Deactivated by Admin Panel
      if (array_key_exists('is_deactivated', $postData) and $deactivated === 0) {
        return $this->activateDivideBuyExtension($storeId, $request);
      }

      return $this->forwardRequestByMethod($request);
    } catch (Throwable $exception) {
      return $this->responseHelper->sendJsonResponse([
          'error' => 1,
          'success' => 0,
          'message' => $exception->getMessage(),
          'status' => (string) $exception->getCode(),
      ]);
    }
  }

  public function forwardRequestByMethod(DivideBuyRequest $request): ?ResponseInterface
  {
    $this->validateRequestRequiringOrderId($request);

    switch ($request->getMethod()) {
      case 'orderSuccess':
        return $this->_forward('successOrder');
      case 'orderCancel':
        return $this->_forward('cancelOrder');
      case 'orderDelete':
        return $this->_forward('deleteOrder');
      case 'retailerConfigurations':
        return $this->_forward('retailerConfigurations');
      case 'updateCouriers':
        return $this->_forward('updateCourier');
      default:
        throw new InvalidParameterException('Method not found', 405);
    }
  }

  /**
   * Check auth and token parameters are valid
   *
   * @param  DivideBuyRequest  $request
   *
   * @return array
   */
  public function validateRequest(DivideBuyRequest $request): array
  {
    $request_token = $request->getStoreToken();
    $request_auth = $request->getStoreAuth();

    if($request_token !== $this->configHelper->getStoreToken($request->getRetailerStoreCode()) && $request_auth !== $this->configHelper->getAuthenticationKey($request->getRetailerStoreCode())){
        throw new InvalidParameterException('Please check the entered the token and authentication key', 404);
    }

    if($request_token !== $this->configHelper->getStoreToken($request->getRetailerStoreCode())){
        throw new InvalidParameterException('There is a problem with Store Token. Please check the entered store token', 404);
    }

    if($request_auth !== $this->configHelper->getAuthenticationKey($request->getRetailerStoreCode())){
        throw new InvalidParameterException('There is a problem with Authentication Key. Please check the entered Authentication key', 404);
    }

    $postData = $request->getContent()->getArrayCopy();

    if (empty($postData)) {
      throw new InvalidParameterException('There is a problem in retrieving data', 406);
    }

    return $postData;
  }

  /**
   * Ensure there is a valid order before forwarding the request
   *
   * @param  DivideBuyRequest  $request
   *
   * @return bool
   */
  public function validateRequestRequiringOrderId(DivideBuyRequest $request): bool
  {
    $method = $request->getMethod();
    $methodRequiringOrderId = ['orderSuccess', 'orderCancel', 'orderDelete'];

    if (!in_array($method, $methodRequiringOrderId)) {
      return true;
    }

    $storeOrderId = $request->getParameterValue('store_order_id');
    $order = $this->orderModel->load($storeOrderId);

    if (!$order or !$order->getId()) {
      throw new InvalidParameterException('Order not found', 404);
    }

    $paymentMethod = $order->getPayment()->getMethod();

    if ($paymentMethod !== DivideBuy::DIVIDEBUY_PAYMENT_CODE) {
      throw new InvalidParameterException('Non-DivideBuy order', 402);
    }

    return true;
  }

  /**
   *  Checking if allowed IPs are coming in postData or not.
   *
   * @param $postData
   * @param $allowedIps
   * @param $storeId
   */
  public function checkAllowedIps($postData, $allowedIps, $storeId)
  {
    if (!array_key_exists('allowedIps', $postData)) {
      return;
    }

    $env = $this->responseHelper->getConfigHelper()->getEnvironment($storeId);

    if ($env === 'staging' && !$allowedIps) {
      throw new InvalidParameterException(
          'Update allowed IPs failed as retailer is pointed to sandbox environment.',
          422
      );
    }

    $this->responseHelper->getConfigHelper()->saveConfig(
        XmlFilePaths::XML_PATH_DIVIDEBUY_ALLOWED_IP,
        $postData['allowedIps'],
        $storeId ? 'stores' : 'default',
        $storeId ? $storeId : 0
    );
  }

  public function activateDivideBuyExtension($storeId, DivideBuyRequest $request)
  {
    $flag = (int) $this->responseHelper->getConfigHelper()->getGlobalDeactivateFlag($storeId);

    if ($flag !== 1) {
      return $this->responseHelper->sendJsonResponse([
          'error' => 0,
          'success' => 1,
          'message' => 'OK',
      ]);
    }

    $scope = $storeId ? 'stores' : 'default';
    $storeId = $storeId ? $storeId : 0;

    $this->responseHelper->getConfigHelper()
        ->saveConfig(
            XmlFilePaths::XML_PATH_TOKEN_NUMBER,
            $request->getParameterValue('store_token'),
            $scope,
            $storeId
        )->saveConfig(
            XmlFilePaths::XML_PATH_AUTH_NUMBER,
            $request->getParameterValue('store_authentication'),
            $scope,
            $storeId
        )->saveConfig(
            XmlFilePaths::XML_PATH_RETAILER_ID,
            $request->getParameterValue('retailer_id'),
            $scope,
            $storeId
        )->saveConfig(
            XmlFilePaths::XML_PATH_DIVIDEBUY_GLOBAL_DEACTIVATED,
            0,
            $scope,
            $storeId
        );

    return $this->responseHelper->sendJsonResponse([
        'error' => 0,
        'success' => 1,
        'message' => 'Retailer is Activated.',
    ]);
  }
}
