<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class VerifyToken extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  /**
   * @var CheckoutSession | \Dividebuy\Common\CheckoutSession
   */
  protected CheckoutSession $checkoutSession;

  private ApiHelper $apiHelper;

  public function __construct(Context $context, CheckoutSession $checkoutSession, ApiHelper $apiHelper)
  {
    $this->checkoutSession = $checkoutSession;
    $this->apiHelper = $apiHelper;

    parent::__construct($context);
  }

  /**
   * Used to check zipcode is valid and shipping rates are changed or not.
   *
   * @return ResponseInterface|ResultInterface
   */
  public function execute()
  {
    $phoneOrderToken = $this->getRequest()->getParam('token');

    if (!empty($phoneOrderToken)) {
      $request = ['token' => $phoneOrderToken];
      $response = $this->apiHelper->getSdkApi()->verifyTokenPermission($request);

      if (!isset($response['error']) && empty($response['error'])) {
        //Checking for phone order session
        $phoneOrderTokenSession = $this->checkoutSession->getDividebuyPhoneOrderToken();
        if ($phoneOrderTokenSession) {
          $this->checkoutSession->unsDividebuyPhoneOrderToken();
        }

        //Setting up a new session
        $this->checkoutSession->setDividebuyPhoneOrderToken($phoneOrderToken);

        // Checking if checkout is available or not.
        if ($response['is_checkout_allowed'] === false) {
          // Checking for existing session for DivideBuy checkout.
          $divideBuyCheckoutSession = $this->checkoutSession->getDividebuyCheckoutSession();
          if ($divideBuyCheckoutSession) {
            $this->checkoutSession->unsDividebuyCheckoutSession();
          }

          // Setting up new checkout session.
          $this->checkoutSession->setDividebuyCheckoutSession('no');
        }

        $this->_redirect('/?token='.$phoneOrderToken);
      }
    }

    //Redirecting to home page
    return $this->_redirect('/');
  }
}
