<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Constants\XmlFilePaths;
use Dividebuy\Common\DivideBuyRequest;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\ResponseHelper;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Store\Model\Store;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;

class RetailerConfigurations extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected Store $storeModel;

  protected ResponseHelper $responseHelper;

  private ApiHelper $apiHelper;

  /**
     * @var \Magento\Framework\Serialize\Serializer\Serialize
     */
    private $serialize;

    private TypeListInterface $cacheTypeList;
    private Pool $cacheFrontendPool;

  public function __construct(
      Context $context,
      Store $storeModel,
      ResponseHelper $responseHelper,
      ApiHelper $apiHelper,
      \Magento\Framework\Serialize\Serializer\Serialize $serialize,
      TypeListInterface $cacheTypeList,
      Pool $cacheFrontendPool
  ) {
    $this->storeModel = $storeModel;
    $this->responseHelper = $responseHelper;
    $this->apiHelper = $apiHelper;
    $this->serialize = $serialize;
    $this->cacheTypeList = $cacheTypeList;
    $this->cacheFrontendPool = $cacheFrontendPool;

    parent::__construct($context);
  }

  public function execute()
  {
    $post = $this->responseHelper->getRequestStreamData();
    $request = new DivideBuyRequest($post);
    $storeCode = $request->getRetailerStoreCode();
    $store = $this->storeModel->load($storeCode);
    $storeId = $store->getStoreId();
    $result = $this->addRetailerConfigurations($request, $storeId);
    // Clean cache after updating configurations
    $this->cleanCache();
    return $this->responseHelper->sendJsonResponse($result);
  }

  /**
   * Adds retailer configurations
   *
   * @param  DivideBuyRequest  $request
   * @param  mixed  $storeId
   *
   * @return array
   */
  public function addRetailerConfigurations(DivideBuyRequest $request, $storeId): array
  {
    $deactivated = (int) $request->getParameterValue('is_deactivated');
    $scope = $storeId ? 'stores' : 'default';
    $storeId = $storeId ? $storeId : 0;
    $configHelper = $this->responseHelper->getConfigHelper();

    if ($deactivated === 1) {
      $configHelper->saveConfig(
          XmlFilePaths::XML_PATH_TOKEN_NUMBER,
          null,
          $scope,
          $storeId
      )->saveConfig(
          XmlFilePaths::XML_PATH_AUTH_NUMBER,
          null,
          $scope,
          $storeId
      )->saveConfig(
          XmlFilePaths::XML_PATH_RETAILER_ID,
          null,
          $scope,
          $storeId
      )->saveConfig(
          XmlFilePaths::XML_PATH_DIVIDEBUY_EXTENSION_STATUS,
          0,
          $scope,
          $storeId
      )->saveConfig(
          XmlFilePaths::XML_PATH_DIVIDEBUY_GLOBAL_DEACTIVATED,
          1,
          $scope,
          $storeId
      );

      return [
          'error' => 0,
          'success' => 1,
          'status' => 'ok',
      ];
    }

    $configurationDetails = (array) $request->getParameterValue('retailerConfigurationDetails');
    $instalments = [];
    $installmentValues = [];
    $instalments_ibc = [];
    $installmentValues_ibc = [];
    $installmentValues_ibc_max = [];
    $flag = false;

    foreach ($configurationDetails as $configurationDetail) {
      $value = $configurationDetail['value'];
      $field = $configurationDetail['key'];
      $path = 'dividebuy/global/'.$field;

      switch ($configurationDetail['type']) {
        case 'global':
          switch ($configurationDetail['key']) {
            case 'excludePostCodes':
              $path = XmlFilePaths::XML_PATH_DIVIDEBUY_EXCLUDEPOSTCODE;
              break;
            case 'taxClass':
              $path = 'dividebuy/global/tax_class';
              break;
            case 'flagToDeleteOrders':
              $path = 'dividebuy/global/flag_to_delete_orders';
              $value = (string) $value;
              break;
            case 'inStoreCollection':
              $path = 'dividebuy/global/retailor_auto_checkout';
              $value = (string) $value;
              break;
              case 'canSendCommencementNotice':
              $path = 'dividebuy/global/dividebuy_flag_can_send_commencement_notice';
              $value = (string) $value;
              break;
            case 'representativeAPR':
              $path = 'dividebuy/global/dividebuy_representative_APR';
              $value = (string) $value;
              break;
            case 'averageIbcTermLength':
              $path = 'dividebuy/global/dividebuy_average_ibc_term_length';
              $value = (string) $value;
              break;
            case 'averageIbcOrderValue':
              $path = 'dividebuy/global/dividebuy_average_ibc_order_value';
              $value = (string) $value;
              break;
            case 'termsAndConditionUrl':
              $path = 'dividebuy/global/terms_condition_url';
              $value = (string) $value;
              break;
            default:
              continue 3;
          }
          $configHelper->saveConfig($path, $value, $scope, $storeId);
          $flag = true;
          break;
        case 'themes':
          $configHelper->saveConfig($path, $value, $scope, $storeId);
          $flag = true;
          break;
        case 'instalments':
          $instalments[] = [
              'key' => $configurationDetail['key'],
              'value' => $configurationDetail['value'],
              'interest_rate' => $configurationDetail['interest_rate'] ? $configurationDetail['interest_rate'] : "0",
              'apr' => $configurationDetail['apr'] ? $configurationDetail['apr'] : "0",
              'min' => $configurationDetail['min'] ? $configurationDetail['min'] : "0",
              'max' => $configurationDetail['max'] ? $configurationDetail['max'] : "0",
          ];
          $installmentValues[] = $configurationDetail['value'];
          $installmentValues_max[] = $configurationDetail['max'];
          $flag = true;
          break;
          case 'instalments-ibc':
          $instalments_ibc[] = [
              'key' => $configurationDetail['key'],
              'value' => $configurationDetail['value'],
              'interest_rate' => $configurationDetail['interest_rate'] ? $configurationDetail['interest_rate'] : "0",
              'apr' => $configurationDetail['apr'] ? $configurationDetail['apr'] : "0",
              'min' => $configurationDetail['min'] ? $configurationDetail['min'] : "0",
              'max' => $configurationDetail['max'] ? $configurationDetail['max'] : "0",
          ];
          $installmentValues_ibc[] = $configurationDetail['value'];
          $installmentValues_ibc_max[] = $configurationDetail['max'];
          $flag = true;
          break;
      }
      
    }

    $this->saveInstallments($instalments, $installmentValues, $installmentValues_max, $scope, $storeId);
    $this->saveIbcInstallments($instalments_ibc, $installmentValues_ibc, $installmentValues_ibc_max, $scope, $storeId);

   if (isset($instalments_ibc) && isset($instalments)) {
      $allValues = array();
      $instalments_data = array_merge($instalments_ibc, $instalments);
      for ($k = 0; $k < sizeof($instalments_data); $k++) {
         $allValues[] = $instalments_data[$k]['value'];
         $max_value[] = $instalments_data[$k]['max'];
      }
      
      if (!empty(min($allValues))) {
         $minValue = min($allValues);
         $this->responseHelper->getConfigHelper()
             ->saveConfig(XmlFilePaths::XML_PATH_ORDER_MIN_AMOUNT, $minValue, $scope, $storeId);
      }
      if (!empty(max($max_value))) {
         $maxValue = max($max_value);
         $this->responseHelper->getConfigHelper()
             ->saveConfig(XmlFilePaths::XML_PATH_ORDER_MAX_AMOUNT, $maxValue, $scope, $storeId);
      }
   }

    return [
        'error' => $flag ? 0 : 1,
        'success' => $flag ? 1 : 0,
        'response_status' => $flag ? '200' : '404',
        'status' => $flag ? 'ok' : '',
    ];
  }

   public function saveInstallments($instalments, $installmentValues, $installmentValues_max, $scope, $storeId): bool
   {
      if (!$instalments) {
         return false;
      }

      $minValue = min($installmentValues);
      $maxValue = max($installmentValues_max);

      if ($minValue) {
         $this->responseHelper->getConfigHelper()
             ->saveConfig(XmlFilePaths::XML_PATH_ORDER_MIN_AMOUNT, $minValue, $scope, $storeId);
      }

      if ($maxValue) {
         $this->responseHelper->getConfigHelper()
             ->saveConfig(XmlFilePaths::XML_PATH_ORDER_MAX_AMOUNT, $maxValue, $scope, $storeId);
      }

      $this->responseHelper->getConfigHelper()
        ->saveConfig(DivideBuy::DIVIDEBUY_INSTALMENTS, $this->serialize->serialize($instalments), $scope, $storeId);

      return true;
   }

   public function saveIbcInstallments($instalments_ibc, $installmentValues_ibc, $installmentValues_ibc_max, $scope, $storeId): bool
   {
      if (!$instalments_ibc) {
         $this->responseHelper->getConfigHelper()
        ->saveConfig(DivideBuy::DIVIDEBUY_IBC_INSTALMENTS, NULL, $scope, $storeId);
         return false;
      }

      $minValue = min($installmentValues_ibc);
      $maxValue = max($installmentValues_ibc_max);

      if ($minValue) {
         $this->responseHelper->getConfigHelper()
          ->saveConfig(XmlFilePaths::XML_PATH_ORDER_MIN_AMOUNT, $minValue, $scope, $storeId);
      }

      if ($maxValue) {
         $this->responseHelper->getConfigHelper()
          ->saveConfig(XmlFilePaths::XML_PATH_ORDER_MAX_AMOUNT, $maxValue, $scope, $storeId);
      }

      $this->responseHelper->getConfigHelper()
        ->saveConfig(DivideBuy::DIVIDEBUY_IBC_INSTALMENTS, $this->serialize->serialize($instalments_ibc), $scope, $storeId);

      return true;
   }

   private function cleanCache(): void
    {
        $types = [
            'config',
            'layout',
            'block_html',
            'collections',
            'reflection',
            'db_ddl',
            'compiled_config',
            'eav',
            'customer_notification',
            'config_integration',
            'config_integration_api',
            'full_page',
            'translate',
            'config_webservice',
        ];

        foreach ($types as $type) {
            $this->cacheTypeList->cleanType($type);
        }

        foreach ($this->cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }

}
