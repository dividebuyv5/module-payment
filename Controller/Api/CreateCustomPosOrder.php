<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Exception\InvalidParameterException;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\ResponseHelper;
use Dividebuy\Payment\Helper\Data as PaymentData;
use DivideBuySdk\Request\Incoming\CreateCustomPosOrderRequest;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

class CreateCustomPosOrder extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected ResponseHelper $responseHelper;

  protected PaymentData $paymentHelper;

  protected ProductRepositoryInterface $productRepository;

  private ApiHelper $apiHelper;

  /**
   * Constructor for POS order API.
   *
   * @param  Context  $context
   * @param  PaymentData  $paymentHelper
   * @param  ResponseHelper  $responseHelper
   * @param  ApiHelper  $apiHelper
   * @param  ProductRepositoryInterface  $productRepository
   */
  public function __construct(
      Context $context,
      PaymentData $paymentHelper,
      ResponseHelper $responseHelper,
      ApiHelper $apiHelper,
      ProductRepositoryInterface $productRepository
  ) {
    $this->paymentHelper = $paymentHelper;
    $this->responseHelper = $responseHelper;
    $this->productRepository = $productRepository;
    $this->apiHelper = $apiHelper;

    parent::__construct($context);
  }

  /**
   * This function will be executed once Activate POS API will be called from DivideBuy.
   *
   * @return ResponseInterface|ResultInterface
   */
  public function execute()
  {
    $api = $this->apiHelper->getSdkApi();
    $result = $api->activatePosSystem([$this, 'handleCreatePosOrderRequest']);

    if (!empty($result['error'])) {
      $this->responseHelper->debugResponse($result, 'Custom Pos Order');
    }

    return $this->responseHelper->sendJsonResponse($result);
  }

  /**
   * @param  CreateCustomPosOrderRequest  $request
   *
   * @return array
   *
   * @throws CouldNotSaveException
   * @throws LocalizedException
   * @throws NoSuchEntityException
   */
  public function handleCreatePosOrderRequest(CreateCustomPosOrderRequest $request): array
  {
    $customerEmail = $request->getCustomerEmail();
    // Validation of postData values.
    if (!$customerEmail || !filter_var($customerEmail, FILTER_VALIDATE_EMAIL)) {
      throw new InvalidParameterException('Customer email is blank or invalid', 422);
    }

    // Checking of order time is invalid.
    if (!$request->getOrderTime()) {
      throw new InvalidParameterException('Order time is blank or invalid', 422);
    }

    // Checking for blank address fields.
    foreach ($request->getAddress()->getArrayCopy() as $addressKey => $addressValue) {
      if ($addressKey !== 'house_name' && $addressKey !== 'house_number' && $addressKey !== 'address2' && !$addressValue) {
        throw new InvalidParameterException($addressKey.' is a required field.', 422);
      }
    }

    // Loading DivideBuy POS product.
    $product = $this->productRepository->get(DivideBuy::DIVIDEBUY_POS_PRODUCT);
    if ($product->getStatus() === 2) {
      throw new InvalidParameterException('DivideBuy POS prouduct is disabled.', 422);
    }

    $orderTotal = $request->getParameterValue('order_total');

    if (!is_numeric($orderTotal) && !is_float($orderTotal)) {
      throw new InvalidParameterException('Order total is invalid.', 422);
    }

    // Placing POS order to show in retailer sales grid.
    $createPosOrder = $this->paymentHelper->createPosOrder($request);

    return [
        'error' => 0,
        'success' => 1,
        'order_id' => $createPosOrder->getID(),
        'order_increment_id' => $createPosOrder->getIncrementID(),
        'status' => 'OK',
    ];
  }
}
