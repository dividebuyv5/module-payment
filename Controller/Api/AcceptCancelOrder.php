<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Exception\InvalidParameterException;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\OrderHelper;
use Dividebuy\Common\Utility\ResponseHelper;
use Dividebuy\Common\Utility\StoreConfigHelper;
use DivideBuySdk\Request\Incoming\CancelOrderRequest;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class AcceptCancelOrder extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  private ResponseHelper $responseHelper;

  private ApiHelper $apiHelper;

  private StoreConfigHelper $storeConfigHelper;

  private OrderHelper $orderHelper;

  public function __construct(
      Context $context,
      ResponseHelper $responseHelper,
      StoreConfigHelper $storeConfigHelper,
      ApiHelper $apiHelper,
      OrderHelper $orderHelper
  ) {
    $this->responseHelper = $responseHelper;
    $this->apiHelper = $apiHelper;
    $this->storeConfigHelper = $storeConfigHelper;
    $this->orderHelper = $orderHelper;

    parent::__construct($context);
  }

  /**
   * Used to change status of cancelled order to pending.
   *
   * @return ResponseInterface|ResultInterface
   */
  public function execute()
  {
    $api = $this->apiHelper->getSdkApi();
    $response = $api->cancelOrder([$this, 'handleCancelOrderRequest']);

    return $this->responseHelper->sendJsonResponse($response);
  }

  public function handleCancelOrderRequest(CancelOrderRequest $request): array
  {
    $postData = $request->getContent();
    $this->validateRequest($postData);

    $orderId = $request->getOrderId();
    $order = $this->orderHelper->loadOrderById($orderId);

    if (!$order || empty($order->getId())) {
      throw new InvalidParameterException('Order not found.', 402);
    }

    if (!$this->orderHelper->validateDivideBuyOrder($order)) {
      throw new InvalidParameterException('Non-Dividebuy order.', 402);
    }

    $successOrder = $postData['successOrder'] === 1;

    return $this->orderHelper->processCancelOrderRequest($order, $successOrder);
  }

  public function validateRequest($postData): bool
  {
    if (empty($postData)) {
      throw new InvalidParameterException('There is a problem in retrieving data.', 402);
    }

    if (empty($postData['orderId'])) {
      throw new InvalidParameterException('Please mention order id.', 402);
    }

    if (empty($postData['storeToken']) || empty($postData['storeAuthentication']) || empty($postData['retailerStoreCode'])) {
      throw new InvalidParameterException('Authentication, token and store code are required.', 402);
    }

    return true;
  }
}
