<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Magento\Framework\DataObject;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\ResponseHelper;
use Exception;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Checkout\Model\Session as SessionModel;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Sales\Model\Order;

class CancelSoftSearchOrder extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected $request; 

  protected SessionModel $checkoutSession;

  protected Order $orderModel;

  private ResponseHelper $responseHelper;

  public function __construct(
      \Magento\Framework\App\RequestInterface $request,
      Context $context,
      CheckoutSession $checkoutSession,
      Order $orderModel,
      ResponseHelper $responseHelper
  ) {
    $this->request = $request; 
    $this->checkoutSession = $checkoutSession;
    $this->orderModel = $orderModel;
    $this->responseHelper = $responseHelper;

    parent::__construct($context);
  }

  /**
   * Used to check zipcode is valid and shipping rates are changed or not.
   *
   * @return ResponseInterface|ResultInterface
   *
   * @throws Exception
   */
  public function execute()
  {
    // Getting splash key from request.
    $splashKey = (new DataObject($this->request->getPost()->toArray()))->getDataByKey('splashkey');
    $getOrderSplashKey = $this->responseHelper->getConfigHelper()->decodeSplashKey($splashKey);
    // Load order from store.
    $orderId = empty($getOrderSplashKey[1]) ? null : $getOrderSplashKey[1];
    $order = $this->orderModel->load($orderId);

    // Check if order exists or not.
    if (!$order || empty($order->getId())) {
      $result = [
          'error' => 1,
          'success' => 0,
          'message' => 'Order not found.',
          'status' => '402',
      ];

      return $this->responseHelper->sendJsonResponse($result);
    }

    // Load quote of an order.
    $this->checkoutSession->restoreQuote();

    // Delete an order.
    $this->responseHelper->getRegistry()->register('isSecureArea', true);
    if ($order->delete()) {
      $result = [
          'error' => 1,
          'success' => 0,
          'message' => 'Order has been successfully cancelled.',
          'status' => '402',
      ];
    } else {
      $result = [
          'error' => 1,
          'success' => 0,
          'message' => 'There is problem in retrieving data.',
          'status' => '422',
      ];
    }

    return $this->responseHelper->sendJsonResponse($result);
  }
}
