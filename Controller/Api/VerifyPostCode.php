<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Controller\Api;

use Dividebuy\Common\AbstractActionController;
use Dividebuy\Common\ApiHelper;
use Dividebuy\Common\Traits\CsrfAwareActionTrait;
use Dividebuy\Common\Utility\CartHelper;
use Dividebuy\Common\Utility\OrderHelper;
use Dividebuy\Common\Utility\ResponseHelper;
use Dividebuy\Common\Utility\StoreConfigHelper;
use DivideBuySdk\Request\Incoming\VerifyPostcodeRequest;
use InvalidArgumentException;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

class VerifyPostCode extends AbstractActionController implements CsrfAwareActionInterface
{
  use CsrfAwareActionTrait;

  protected CartHelper $cartHelper;

  protected ResponseHelper $responseHelper;

  private ApiHelper $apiHelper;

  private StoreConfigHelper $configHelper;

  private OrderHelper $orderHelper;

  /**
     * @var \Magento\Framework\Json\Helper\Data
     */
  protected $_jsonHelper;

  /**
     * Magento\Store\Model\Store
     */
    protected $_storeModel;

  public function __construct(
      Context $context,
      CartHelper $cartHelper,
      ResponseHelper $responseHelper,
      ApiHelper $apiHelper,
      OrderHelper $orderHelper,
      \Magento\Framework\Json\Helper\Data $jsonHelper,
      \Magento\Store\Model\Store $storeModel
  ) {
    $this->cartHelper = $cartHelper;
    $this->responseHelper = $responseHelper;
    $this->apiHelper = $apiHelper;
    $this->configHelper = $responseHelper->getConfigHelper();
    $this->orderHelper = $orderHelper;
    $this->_jsonHelper  = $jsonHelper;
    $this->_storeModel  = $storeModel;

    parent::__construct($context);
  }

  /**
   * Used to check zipcode is valid and shipping rates are changed or not.
   *
   * @return ResponseInterface|ResultInterface
   */
  public function execute()
  {
    $post     = trim(file_get_contents("php://input"));
    $postData = $this->_jsonHelper->jsonDecode($post);
    $storeToken = $postData['store_token'];
    $storeAuthentication = $postData['store_authentication'];
    $storeCode = $postData['retailer_store_code'];
    $store = $this->_storeModel->load($storeCode);
    $storeId = $store->getStoreId();
    $api = $this->apiHelper->getSdkApi($env = '', $storeId);
    $result = $api->verifyPostcode([$this, 'handleVerifyPostcodeRequest']);

    return $this->responseHelper->sendJsonResponse($result);
  }

  public function handleVerifyPostcodeRequest(VerifyPostcodeRequest $request): array
  {
    $zipcode = $request->getUserPostcode();
    $zipcodeVerification = $this->configHelper->isPostCodeDeliverable($zipcode);
    if (!$zipcodeVerification) {
      $exception = new InvalidArgumentException(
          'Unfortunately we are unable to deliver to this postcode. Please call our Customer Service team for more details: 0800 085 0885.',
          422
      );
      $this->responseHelper->debugResponse('', __METHOD__, $exception, $request->getContent()->getArrayCopy());
      throw $exception;
    }
    $store_code = $request->getRetailerStoreCode();
    $orderId = $request->getOrderId() ?: (int) $this->getRequest()->getParam('order_id');
    $validate_store = $this->orderHelper->validateOrderStore($orderId,$store_code);
    $orderIsValid = $this->loadValidOrder($orderId);

    if ($orderIsValid) {
      $shippingChanged = false;
      $country = 'GB';
      $order = $this->cartHelper->getOrderById($orderId);
      $isDivideBuyOrder = $this->orderHelper->validateDividebuyOrder($order);
      if (!$isDivideBuyOrder) {
        throw new InvalidArgumentException('Non-Dividebuy order.', 402);
      }
      // Loading quote from order
      $quoteId = $this->cartHelper->getOrderById($orderId)->getQuoteId();
      $quote = $this->cartHelper->loadQuote($quoteId);

      $orderShippingMethod = $order->getShippingMethod();
      $orderShippingAmount = $order->getShippingAmount();

      // Setting shipping address zipcode and country
      $address = $quote->getShippingAddress();
      $address->setCountryId($country)->setPostcode($zipcode);
      $quote->save();

      $quote->getShippingAddress()->setCollectShippingRates(true);
      $quote->getShippingAddress()->collectShippingRates();

      $rates = $quote->getShippingAddress()->getShippingRatesCollection();
      foreach ($rates as $rate) {
        if (!$rate->getRateId()) {
          if ($rate->getCode() === $orderShippingMethod) {
            // comparing price of shipping methods
            if (number_format((float)$rate->getPrice(), 2) !== number_format((float)$orderShippingAmount, 2)) {
              $shippingChanged = true;
            }

            break;
          }
        }
      }
      // Checking if shipping data has been changed or not.
      if ($shippingChanged) {
        throw new InvalidArgumentException('There is change in shipping methods.', 408);
      }

      return [
          'error' => 0,
          'success' => 1,
          'message' => 'ok',
          'status' => '200',
      ];
    }

    throw new InvalidArgumentException('Order not found', 404);
  }

  /**
   * User to load order and check order is valid or not.
   *
   * @param $orderId
   *
   * @return bool
   */
  private function loadValidOrder($orderId = null): bool
  {
    if (!$orderId) {
      return false;
    }

    $order = $this->cartHelper->getOrderById($orderId);

    if ($order->getId()) {
      return true;
    }

    return false;
  }
}
