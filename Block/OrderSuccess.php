<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Block;

use Dividebuy\Common\CheckoutSessionInterface;
use Magento\Checkout\Block\Onepage\Success;
use Magento\Checkout\Model\Session;
use Magento\Customer\Model\Context;
use Magento\Framework\App\Http\Context as MagentoContext;
use Magento\Framework\View\Element\Template\Context as TemplateContext;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Config;

class OrderSuccess extends Success
{
  /**
   * @var Order
   */
  protected Order $orderModel;

  /**
   * OrderSuccess constructor.
   *
   * @param  TemplateContext  $context
   * @param  Session | CheckoutSessionInterface  $checkoutSession
   * @param  Config  $orderConfig
   * @param  MagentoContext  $httpContext
   * @param  Order  $orderModel
   * @param  array  $data
   */
  public function __construct(
      TemplateContext $context,
      Session $checkoutSession,
      Config $orderConfig,
      MagentoContext $httpContext,
      Order $orderModel,
      array $data = []
  ) {
    parent::__construct($context, $checkoutSession, $orderConfig, $httpContext, $data);

    $this->_isScopePrivate = true;
    $this->orderModel = $orderModel;
  }

  /**
   * Render additional order information lines and return result html.
   *
   * @return string
   */
  public function getAdditionalInfoHtml()
  {
    return $this->_layout->renderElement('order.success.additional.info');
  }

  /**
   * get dividebuy order Id from checkout session.
   *
   * @return mixed
   */
  public function getOrderId()
  {
    $orderId = $this->getCheckoutSession()->getLastOrderId();
    $order = $this->orderModel->load($orderId);

    return $order->getIncrementId();
  }

  /**
   * get divide buy order increment Id from checkout session.
   *
   * @return mixed
   */
  public function getOrderIncrementId()
  {
    $orderId = $this->getCheckoutSession()->getLastOrderId();
    $order = $this->orderModel->load($orderId);

    return $order->getIncrementId();
  }

  /**
   * Get CheckoutSession Object.
   *
   * @return Session | CheckoutSessionInterface
   */
  public function getCheckoutSession()
  {
    return $this->_checkoutSession;
  }

  /**
   * Initialize data and prepare it for output.
   *
   * @return mixed
   */
  protected function _beforeToHtml()
  {
    $this->prepareBlockData();

    return parent::_beforeToHtml();
  }

  /**
   * Prepares block of data.
   */
  protected function prepareBlockData()
  {
    $orderId = $this->getCheckoutSession()->getLastOrderId();
    $order = $this->orderModel->load($orderId);
    $this->addData(
        [
            'is_order_visible' => $this->isVisible($order),
            'view_order_url' => $this->getUrl(
                'sales/order/view/',
                ['order_id' => $order->getEntityId()]
            ),
            'print_url' => $this->getUrl(
                'sales/order/print',
                ['order_id' => $order->getEntityId()]
            ),
            'can_print_order' => $this->isVisible($order),
            'can_view_order' => $this->canViewOrder($order),
            'order_id' => $order->getIncrementId(),
        ]
    );
  }

  /**
   * Is order visible or not.
   *
   * @param  Order  $order
   *
   * @return bool
   */
  protected function isVisible(Order $order): bool
  {
    return !in_array(
        $order->getStatus(),
        $this->_orderConfig->getInvisibleOnFrontStatuses()
    );
  }

  /**
   * Can view order.
   *
   * @param  Order  $order
   *
   * @return bool
   */
  protected function canViewOrder(Order $order): bool
  {
    return $this->httpContext->getValue(Context::CONTEXT_AUTH) && $this->isVisible($order);
  }
}
