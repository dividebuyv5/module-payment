<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Block\Sales\Order;

use Magento\Sales\Block\Order\Recent as RecentOrder;

class Recent extends RecentOrder
{
  protected function _construct()
  {
    parent::_construct();

    $orders = $this->_orderCollectionFactory->create()
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('customer_id', $this->_customerSession->getCustomerId())
        ->addAttributeToFilter('status', ['in' => $this->_orderConfig->getVisibleOnFrontStatuses()])
        ->addAttributeToSort('created_at', 'desc')->setPageSize('5')
        ->load();

    $this->setOrders($orders);
  }

  /**
   * Used to get HTML.
   *
   * @return string
   */
  protected function _toHtml()
  {
    $this->setModuleName($this->extractModuleName(RecentOrder::class));

    return parent::_toHtml();
  }
}
