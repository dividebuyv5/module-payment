<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Block\Form;

use Magento\OfflinePayments\Block\Form\AbstractInstruction;

/**
 * Block for Cash On Delivery payment method form.
 */
class Dbpayment extends AbstractInstruction
{
  /**
   * Cash on delivery template.
   *
   * @var string
   */
  protected $_template = 'Dividebuy_Payment::form/dbpayment.phtml';
}
