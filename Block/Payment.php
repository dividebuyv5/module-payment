<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Block;

use Dividebuy\Common\Utility\CartHelper;
use Dividebuy\Common\Utility\ProductHelper;
use Dividebuy\Common\Utility\StoreConfigHelper;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

class Payment extends Template
{
  protected CartHelper $cartHelper;

  protected StoreConfigHelper $configHelper;

  private ProductHelper $productHelper;

  public function __construct(
      Context $context,
      CartHelper $cartHelper,
      StoreConfigHelper $configHelper,
      ProductHelper $productHelper,
      array $data = []
  ) {
    $this->cartHelper = $cartHelper;
    $this->configHelper = $configHelper;
    $this->productHelper = $productHelper;

    parent::__construct($context, $data);
  }

  public function getCartHelper(): CartHelper
  {
    return $this->cartHelper;
  }

  public function getConfigHelper(): StoreConfigHelper
  {
    return $this->configHelper;
  }

  public function getSymbol(): string
  {
    return $this->productHelper->getSymbol();
  }

  public function getCheckoutConfigBlock()
  {
    return $this->cartHelper;
  }

  public function getRetailerHelper()
  {
    return $this->configHelper;
  }
}
