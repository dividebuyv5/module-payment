<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Helper;

use Dividebuy\Common\Constants\DivideBuy;
use Dividebuy\Common\Utility\CartHelper;
use Dividebuy\Common\Utility\OrderHelper;
use Dividebuy\Common\Utility\ProductHelper;
use Dividebuy\Common\Utility\ResponseHelper;
use Dividebuy\Common\Utility\SessionHelper;
use DivideBuySdk\Request\Incoming\CreateCustomPosOrderRequest;
use Exception;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context as HelperContext;
use Magento\Framework\DB\Transaction;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Sales\Model\Order as OrderModel;
use Magento\Sales\Model\Service\InvoiceService;

class Data extends AbstractHelper
{
  protected Context $actionContext;

  protected InvoiceService $invoiceService;

  protected Transaction $transaction;

  protected ResponseHelper $responseHelper;

  private OrderHelper $orderHelper;

  private ProductHelper $productHelper;

  private CartHelper $cartHelper;

  private SessionHelper $sessionHelper;

  public function __construct(
      HelperContext $context,
      Context $actionContext,
      InvoiceService $invoiceService,
      Transaction $transaction,
      ResponseHelper $responseHelper,
      OrderHelper $orderHelper,
      ProductHelper $productHelper,
      CartHelper $cartHelper,
      SessionHelper $sessionHelper
  ) {
    $this->actionContext = $actionContext;
    $this->invoiceService = $invoiceService;
    $this->transaction = $transaction;
    $this->responseHelper = $responseHelper;
    $this->orderHelper = $orderHelper;
    $this->productHelper = $productHelper;
    $this->cartHelper = $cartHelper;
    $this->sessionHelper = $sessionHelper;

    parent::__construct($context);
  }

  /**
   * Generate invoice of given order id.
   *
   * @param $orderId
   *
   * @throws LocalizedException
   * @throws Exception
   * @throws Exception
   */
  public function generateInvoice($orderId)
  {
    $order = $this->orderHelper->loadOrderById($orderId);
    if ($order->canInvoice()) {
      $invoice = $this->invoiceService->prepareInvoice($order);
      $invoice->register();
      $invoice->save();
      $transactionSave = $this->transaction->addObject(
          $invoice
      )->addObject(
          $invoice->getOrder()
      );
      $transactionSave->save();
    }
  }

  public function completeOrder($order, $orderDetails): bool
  {
    $this->orderHelper->completeOrder($order, $orderDetails);

    return true;
  }

  /**
   * This function creates new product for POS.
   *
   * @param $websiteId
   *
   * @return mixed
   *
   * @throws Exception
   */
  public function createPosProduct($websiteId)
  {
    // Checking if product is already exists
    $customPosProductId = $this->getPosProductId();

    if (!$customPosProductId) {
      return $this->productHelper->createPosProduct($websiteId);
    }

    // Enabling custom order product.
    $customProduct = $this->productHelper->loadProduct($customPosProductId);
    $customProduct->setStatus(1);
    $customProduct->save();

    return $customPosProductId;
  }

  /**
   * Function for getting custom POS product id.
   *
   * @return false|int|null
   */
  public function getPosProductId()
  {
    try {
      $product = $this->productHelper->getProductBySku(DivideBuy::DIVIDEBUY_POS_PRODUCT);
      if ($product && !empty($product->getId())) {
        return $product->getId();
      }
    } catch (Exception $e) {
      return false;
    }

    return false;
  }

  /**
   * Function used for activate/deactivate POS shipping.
   *
   * @param  int  $activateFlag
   * @param  int  $storeId
   *
   * @return bool
   */
  public function activateDeactivatePosShipping($activateFlag, $storeId): bool
  {
    $scope = 'stores';

    if ($storeId === '') {
      $storeId = 0;
      $scope = 'default';
    }

    // Updating shipping configuration.
    $this->responseHelper->getConfigHelper()->saveConfig(
        DivideBuy::DIVIDEBUY_XML_PATH_POS_SHIPPING,
        $activateFlag,
        $scope,
        $storeId
    );

    return true;
  }

  /**
   * This function is used for creating DivideBuy POS order.
   *
   * @param  CreateCustomPosOrderRequest  $postData
   *
   * @return OrderModel|null
   *
   * @throws CouldNotSaveException
   * @throws LocalizedException
   * @throws NoSuchEntityException
   */
  public function createPosOrder(CreateCustomPosOrderRequest $postData)
  {
    $store = $this->responseHelper->getConfigHelper()->getStore();
    $storeId = $this->responseHelper->getConfigHelper()->getStoreId();

    // Creating empty cart then assigning quote to store.
    $cartId = $this->cartHelper->createEmptyCart();
    $quote = $this->cartHelper->getCartById($cartId);

    // Storing quote ID into session.
    $this->sessionHelper->getCheckoutSession()->setDividebuyCustomQuoteId($quote->getId());
    $quote->setStore($store);

    // Adding product into quote after updating product price.
    $posProductId = $this->getPosProductId();
    $product = $this->productHelper->loadProduct($posProductId, $storeId);

    // Updating product price
    $product->setPrice($postData->getOrderTotal());
    $product->save();
    $quote->addProduct($product, 1);

    // Assiging address to quote.

    $updatedAddress = $postData->getAddress();
    $addressStreet = $updatedAddress->getHouseName().', '.$updatedAddress->getStreet().', '.$updatedAddress->getAddress2();
    $orderAddress = [
        'firstname' => $updatedAddress['first_name'] ?? '',
        'lastname' => $updatedAddress['last_name'] ?? '',
        'email' => $postData->getCustomerEmail(),
        'street' => ltrim($addressStreet, ','),
        'city' => $updatedAddress->getCity(),
        'country_id' => 'GB',
        'region' => $updatedAddress->getRegion(),
        'postcode' => $updatedAddress->getPostCode(),
        'telephone' => $updatedAddress->getContactNumber(),
    ];
    $quote->getBillingAddress()->addData($orderAddress);
    $shippingAddress = $quote->getShippingAddress()->addData($orderAddress);

    // Adding shipping method to quote.
    $shippingAddress->setCollectShippingRates(true)
        ->collectShippingRates()
        ->setShippingMethod('dividebuyposshipping_dividebuyposshipping');
    $quote->setPaymentMethod(DivideBuy::DIVIDEBUY_PAYMENT_CODE);

    // Assigning user as guest for order.
    $quote->setCheckoutMethod('guest')
        ->setCustomerId(0)
        ->setCustomerEmail($quote->getBillingAddress()->getEmail())
        ->setCustomerIsGuest(true);

    // Set Sales Order Payment
    $quote->getPayment()->importData(['method' => DivideBuy::DIVIDEBUY_PAYMENT_CODE]);

    // Removing discounts from quote
    $quote->setCouponCode('');
    $quote->setAppliedRuleId('');
    $quote->save();

    // Collect Totals
    $quote->collectTotals();

    // Creating order from quote.
    $quote = $this->cartHelper->getCartById($quote->getId());
    $orderId = $this->cartHelper->placeCartOrder($quote->getId());
    $order = $this->orderHelper->loadOrderById($orderId);

    $order->setEmailSent(0);

    // Updating order information.
    $order->setCustomerFirstname($updatedAddress['first_name'] ?? '');
    $order->setCustomerLastname($updatedAddress['last_name'] ?? '');
    $order->setCustomerEmail($postData->getCustomerEmail());
    $order->setCreatedAt($postData->getOrderTime());
    $order->addStatusHistoryComment(
        'Custom Phone Order - DivideBuy: </br>'.$postData->getParameterValue('comment_detail'),
        OrderModel::STATE_PROCESSING
    );

    $order->save();

    // Generating order invoice.
    if ($order->canInvoice()) {
      $invoice = $this->invoiceService->prepareInvoice($order);
      $invoice->register();
      $invoice->save();
      $transactionSave = $this->transaction->addObject(
          $invoice
      )->addObject(
          $invoice->getOrder()
      );
      $transactionSave->save();
    }

    // Clearing quote session.
    $this->sessionHelper->getCheckoutSession()->unsDividebuyCustomQuoteId();

    return $order;
  }

  /**
   * This function is used for checking if shipping method is available for provided quote or not.
   *
   * @param  int  $quoteId
   *
   * @return bool
   */
  public function isShippingMethodAvailable($quoteId): bool
  {
    $quote = $this->cartHelper->loadQuote($quoteId);
    foreach ($quote->getAllItems() as $item) {
      $productSKU = $item->getProduct()->getSKU();
      if ($productSKU === DivideBuy::DIVIDEBUY_POS_PRODUCT) {
        return true;
      }
    }

    return false;
  }

  /**
   * Used to validate the order is dividebuy or not.
   *
   * @param  OrderModel  $order
   *
   * @return bool
   */
  public function validateDividebuyOrder($order): bool
  {
    $payment = $order->getPayment();
    if (!empty($payment)) {
      $method = $payment->getMethodInstance();
      $methodCode = $method->getCode();

      if ($methodCode === DivideBuy::DIVIDEBUY_PAYMENT_CODE) {
        return true;
      }
    }

    return false;
  }
}
