<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Helper;

use Magento\Catalog\Model\Product;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\QuoteManagement;
use Magento\Sales\Model\Order as SaleOrder;

class Order extends AbstractHelper
{
  protected SaleOrder $orderModel;

  protected Product $productModel;

  protected StockItemRepository $stockItemRepository;

  protected QuoteFactory $quoteRepository;

  protected QuoteManagement $quoteManagement;

  public function __construct(
      Context $context,
      Product $productModel,
      SaleOrder $orderModel,
      StockItemRepository $stockItemRepository,
      QuoteFactory $quoteRepository,
      QuoteManagement $quoteManagement
  ) {
    $this->productModel = $productModel;
    $this->stockItemRepository = $stockItemRepository;
    $this->quoteRepository = $quoteRepository;
    $this->orderModel = $orderModel;
    $this->quoteManagement = $quoteManagement;

    parent::__construct($context);
  }

  /**
   * Get product current available stock.
   *
   * @param  object  $order
   *
   * @return string
   *
   * @throws NoSuchEntityException
   */
  public function getProductCurrentStock($order): string
  {
    $stockAvailable = 'true';
    $orderItems = $order->getAllItems();

    foreach ($orderItems as $item) {
      if ((!$item->getParentItemId() && $item->getProductType() !== 'simple') && $item->getProductType() !== 'grouped') {
        continue;
      }

      $productQuantity = $item->getQtyOrdered();
      $product = $this->productModel->load($item->getProductId());
      $productType = $product->getTypeId();
      $stock = $this->stockItemRepository->get($product->getId());
      // Checking if current product is simple and ordered quantity is greater than current stock.
      if (($productType === 'simple' && round($stock->getQty()) < $productQuantity) || !$stock->getIsInStock()) {
        $stockAvailable = 'false';

        break;
      }
    }

    return $stockAvailable;
  }

  /**
   * Generates new order based on current quote.
   *
   * @param $quoteId
   *
   * @return int
   *
   * @throws LocalizedException
   */
  public function createNewOrder($quoteId): int
  {
    $orderId = '';
    $quote = $this->quoteRepository->create()->load($quoteId);
    $shipping_method = $quote->getShippingAddress()->getShippingMethod();
    $shippingAddress = $quote->getShippingAddress();
    if ($shipping_method === 'freeshipping_freeshipping') {
      $shippingAddress->setFreeShipping(1);
    }
    $paymentMethod = $quote->getPayment()->getMethodInstance()->getCode();

    $shippingAddress->setCollectShippingRates(true)
        ->collectShippingRates()
        ->setShippingMethod($shipping_method)
        ->setPaymentMethod($paymentMethod);

    // Set Sales Order Payment
    $quote->getPayment()->importData(['method' => $paymentMethod]);
    // Collect Totals & Save Quote
    $quote->setReservedOrderId('');
    $quote->collectTotals()->save();

    $orderPlaced = $this->quoteManagement->submit($quote);
    $orderPlaced->setEmailSent(0);
    $increment_id = $orderPlaced->getRealOrderId();

    if ($increment_id) {
      $order = $this->orderModel->load($increment_id, 'increment_id');
      $orderId = $order->getId();
      $quote->setIsActive(0)->save();
    }

    return $orderId;
  }
}
