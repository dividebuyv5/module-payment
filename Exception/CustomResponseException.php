<?php

declare(strict_types=1);

namespace Dividebuy\Payment\Exception;

use Magento\Framework\Exception\LocalizedException;

class CustomResponseException extends LocalizedException
{
}
